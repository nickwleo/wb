<?php

include  __DIR__ . '/WilmaConfig.php';

$conn = mysqli_connect($productionDbHost, $productionDbUsername, $productionDbPassword, $productionDbName);
$bing_conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);
$query = "SELECT a.mag_sku,a.cost,a.qty,c.avg_qty_sold,
					c.velocity, c.ordercount, c.margin
					FROM oms_inventory.`master_product` a
					INNER JOIN oms_inventory.product_analysis c
					ON a.mag_sku = c.mag_sku
					WHERE a.magento_sync = 1 AND a.status = 1 AND a.cost > 0 GROUP BY a.mag_sku";
$qry = $conn->query($query);
$allSku = [];
if(mysqli_num_rows($qry) > 0){
    $u=0;
    while($row = mysqli_fetch_assoc($qry)){
        $allSku[$u] = $row;
        $u++;
    }

    $allSkuChnk = array_chunk($allSku,5000);
    foreach($allSkuChnk as $row1){
        $insertQry = "INSERT IGNORE INTO wilma_bing.master_product
						(mag_sku, cost, qty, avg_qty_sold, velocity,
						ordercount, margin) VALUES ";
        $insertQryValues = '';
        foreach($row1 as $rw){
            $magSku = $rw['mag_sku'];
            $cost = $rw['cost'];
            $qty = $rw['qty'];
            $avg_qty_sold = $rw['avg_qty_sold'];
            $velocity = $rw['velocity'];
            $ordercount = $rw['ordercount'];
            $margin = $rw['margin'];
            ;

            $insertQryValues .= "('" .$magSku ."' , '".$cost."' ,
								'".$qty."'  , '" .$avg_qty_sold ."', '" .$velocity."',
								'" .$ordercount."', '" . $margin . "'),";

        }
        $insertQryValues = rtrim($insertQryValues,',');
        $insertQry = $insertQry.$insertQryValues." ON DUPLICATE KEY UPDATE
							cost = VALUES(cost), qty = VALUES(qty),
							avg_qty_sold = VALUES(avg_qty_sold), velocity = VALUES(velocity),
							ordercount = VALUES(ordercount), margin = VALUES(margin)";
        $qryFir = $bing_conn->query($insertQry);
        if(!$qryFir){
            echo "Error in insert qry :: ".mysqli_error($bing_conn);
        }
    }
}
