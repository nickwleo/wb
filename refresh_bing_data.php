<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 21/01/18
 * Time: 8:14 PM
 */

include  __DIR__ . '/WilmaConfig.php';

// Create connection
$conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);

$executionStartTime = microtime(true);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//echo "\r\nConnected successfully\r\n";

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//echo "\r\nConnected successfully\r\n";

$sql = "TRUNCATE TABLE wilma_bing.not_viewed";
if ($conn->query($sql) === TRUE) {
    //echo "Table truncated successfully\r\n";
} else {
    //echo "Error truncating table : " . $conn->error;
}

$sql = "TRUNCATE TABLE wilma_bing.viewed";
if ($conn->query($sql) === TRUE) {
    //echo "Table truncated successfully\r\n";
} else {
    //echo "Error truncating table : " . $conn->error;
}

$sql = "TRUNCATE TABLE wilma_bing.clicked";
if ($conn->query($sql) === TRUE) {
    //echo "Table truncated successfully\r\n";
} else {
    //echo "Error truncating table : " . $conn->error;
}

$sql = "TRUNCATE TABLE wilma_bing.converted";
if ($conn->query($sql) === TRUE) {
    //echo "Table truncated successfully\r\n";
} else {
    //echo "Error truncating table : " . $conn->error;

}

$not_viewed = 0;
$viewed = 0;
$clicked = 0;
$converted = 0;
$bid_summary = 0;

$not_viewed_query = "INSERT IGNORE INTO wilma_bing.not_viewed (item_id, campaign_id, adgroup_id , campaign_name, adgroup_name, impressions, clicks, conversions, cost, average_cpc) VALUES ";
$viewed_query = "INSERT IGNORE INTO wilma_bing.viewed (item_id, campaign_id, adgroup_id , campaign_name, adgroup_name, impressions, clicks, conversions, cost, average_cpc) VALUES ";
$clicked_query = "INSERT IGNORE INTO wilma_bing.clicked (item_id, campaign_id, adgroup_id , campaign_name, adgroup_name, impressions, clicks, conversions, cost, average_cpc) VALUES ";
$converted_query = "INSERT IGNORE INTO wilma_bing.converted (item_id, campaign_id, adgroup_id , campaign_name, adgroup_name, impressions, clicks, conversions, cost, average_cpc) VALUES ";
$bid_summary_query = "INSERT IGNORE INTO wilma_bing.bid_summary (sku, campaign_id ,adgroup_id , campaign_name, adgroup_name, current_bucket, total_impressions, total_clicks, total_conversions , total_cost , average_cpc) VALUES ";

//iterate through a result set of all products in all_time

$conn->query("SET sql_mode='';"); //Fixing group by issue.

$sql = "SELECT a.item_id , a.campaign_name , a.campaign_id , a.adgroup_name , a.adgroup_id , SUM(a.impressions) AS impressions , SUM(a.clicks) AS clicks , SUM(a.conversions) AS conversions , "
    ."ROUND(SUM(a.cost),2) AS cost , ROUND(SUM(a.cost) / SUM(a.clicks) , 2) AS average_cpc FROM wilma_bing.wilma_products_all_time a  GROUP BY a.item_id , a.campaign_id ;";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row

    while($row = $result->fetch_assoc()) {


        if ($row['impressions'] == 0 && $row['clicks'] == 0) {
            //put in not viewed bucket
            if ($not_viewed != 0) $not_viewed_query .= ", ";
            if ($bid_summary != 0) $bid_summary_query .= ", ";
            $not_viewed_query .= "('" . $row['item_id'] . "', '" . $row['campaign_id'] . "' , '" . $row['adgroup_id'] . "' ,  '" . $row['campaign_name'] . "', '" . $row['adgroup_name'] . "', " . $row['impressions'] . ", " . $row['clicks'] . ", " . $row['conversions'] . ", " . $row['cost'] . " , 0.00)";
            $not_viewed++;
            $bid_summary++;
            $bid_summary_query .= "('" . $row['item_id'] . "', '" . $row['campaign_id'] . "', '" . $row['adgroup_id'] . "' , '" . $row['campaign_name'] . "', '" . $row['adgroup_name'] . "', 'not_viewed' , " . $row['impressions'] . ", " . $row['clicks'] . ", " . $row['conversions'] . " , " . $row['cost'] . " , 0.00)";
        }


        else if ($row['impressions'] > 0 && $row['clicks'] == 0) {
            //put in viewed bucket
            if ($viewed != 0) $viewed_query .= ", ";
            if ($bid_summary != 0) $bid_summary_query .= ", ";
            $viewed_query .= "('" . $row['item_id'] . "', '" . $row['campaign_id'] . "' , '" . $row['adgroup_id'] . "', '" . $row['campaign_name'] . "', '" . $row['adgroup_name'] . "', " . $row['impressions'] . ", " . $row['clicks'] . ", " . $row['conversions'] . ", " . $row['cost'] . ", 0.00)";
            $viewed++;
            $bid_summary++;
            $bid_summary_query .= "('" . $row['item_id'] . "', '" . $row['campaign_id'] . "', '" . $row['adgroup_id'] . "', '" . $row['campaign_name'] . "', '" . $row['adgroup_name'] . "', 'viewed', " . $row['impressions'] . ", " . $row['clicks'] . ", " . $row['conversions'] . " , " . $row['cost'] . " , 0.00)";
        }
        else if ($row['clicks'] > 0 && $row['conversions'] == 0) {
            //put in clicked bucket
            if ($clicked != 0) $clicked_query .= ", ";
            if ($bid_summary != 0) $bid_summary_query .= ", ";
            $clicked_query .= "('" . $row['item_id'] . "', '" . $row['campaign_id'] . "' , '" . $row['adgroup_id'] . "' , '" . $row['campaign_name'] . "', '" . $row['adgroup_name'] . "', " . $row['impressions'] . ", " . $row['clicks'] . ", " . $row['conversions'] . ", " . $row['cost'] . ", " . $row['average_cpc'] . ")";
            $clicked++;
            $bid_summary++;
            $bid_summary_query .= "('" . $row['item_id'] . "', '" . $row['campaign_id'] . "', '" . $row['adgroup_id'] . "', '" . $row['campaign_name'] . "', '" . $row['adgroup_name'] . "', 'clicked', " . $row['impressions'] . ", " . $row['clicks'] . ", " . $row['conversions'] . " , " . $row['cost'] . " , " . $row['average_cpc'] . ")";
        }
        else if ($row['conversions'] > 0) {
            //put in converted bucket
            if ($converted != 0) $converted_query .= ", ";
            if ($bid_summary != 0) $bid_summary_query .= ", ";
            $converted_query .= "('" . $row['item_id'] . "', '" . $row['campaign_id'] . "' , '" . $row['adgroup_id'] . "', '" . $row['campaign_name'] . "', '" . $row['adgroup_name'] . "', " . $row['impressions'] . ", " . $row['clicks'] . ", " . $row['conversions'] . ", " . $row['cost'] . ", " . $row['average_cpc'] . ")";
            $converted++;
            $bid_summary++;
            $bid_summary_query .= "('" . $row['item_id'] . "', '" . $row['campaign_id'] . "', '" . $row['adgroup_id'] . "', '" . $row['campaign_name'] . "', '" . $row['adgroup_name'] . "', 'converted', " . $row['impressions'] . ", " . $row['clicks'] . ", " . $row['conversions'] . " , " . $row['cost'] . " , " . $row['average_cpc'] . ")";


        }


    }
} else {
    //echo "0 results";
}



//item_id, campaign_name, adgroup_name, impressions, clicks, conversions, cost, average_cpc


if (mysqli_query($conn, $not_viewed_query)) {
    //echo "\r\nNot viewed bucket created successfully";
} else {
    //echo "Error: " . $not_viewed_query . "<br>" . mysqli_error($conn);
}

if (mysqli_query($conn, $viewed_query)) {
    //echo "\r\nViewed bucket created successfully";
} else {
    //echo "Error: " . $viewed_query . "<br>" . mysqli_error($conn);
}

if (mysqli_query($conn, $clicked_query)) {
    //echo "\r\nClicked bucket created successfully";
} else {
    //echo "Error: " . $clicked_query . "<br>" . mysqli_error($conn);
}

if (mysqli_query($conn, $converted_query)) {
    //echo "\r\nConverted bucket created successfully";
} else {
    //echo "Error: " . $converted_query . "<br>" . mysqli_error($conn);
}

$bid_summary_query = $bid_summary_query." ON DUPLICATE KEY UPDATE current_bucket = VALUES(current_bucket), total_impressions = VALUES(total_impressions), total_clicks = VALUES(total_clicks), total_conversions = VALUES(total_conversions) , total_cost = VALUES(total_cost) , average_cpc = VALUES(average_cpc)  ";

if (mysqli_query($conn, $bid_summary_query)) {
    //echo "\r\nBid summaries created successfully";
} else {
    //echo "Error: " . $bid_summary_query . "<br>" . mysqli_error($conn);
}





//Update conversions

//echo "Start Conversions Update.\r\n\r\n";


$result = $conn->query("SELECT a.item_id , a.campaign_name , a.campaign_id , a.adgroup_id , a.adgroup_name , SUM(a.impressions) AS impressions , SUM(a.clicks) AS clicks , SUM(a.conversions) AS conversions , ROUND(SUM(a.cost),2) AS cost , a.average_cpc
        FROM wilma_bing.wilma_products_all_time a WHERE a.conversions > 0 OR a.clicks > 0  GROUP BY a.item_id , a.campaign_id ");

while ($row = $result->fetch_assoc()) {



    if ((int) $row['conversions'] !== 0 && (int) $row['clicks'] > 0)   {
        $sql = "INSERT IGNORE INTO wilma_bing.conversion_rates (mag_sku, campaign_name, clicks , conversions , campaign_id , adgroup_id , all_time ,  bucket , updated) VALUES (" . $row['item_id'] . ", '" . $row['campaign_name'] . "', " . $row['clicks'] . " , " . $row['conversions'] . " , " . $row['campaign_id'] . " ,'" . $row['adgroup_id'] . "' , ROUND(" . $row['conversions'] . " / ". $row['clicks'] . " , 3) , 'converted' , 1) "
            ."ON DUPLICATE KEY UPDATE updated = IF (clicks <> VALUES(clicks), updated + 1 , updated) , conversions = VALUES(conversions) , clicks = VALUES(clicks) , all_time = VALUES(all_time) , bucket = 'converted' ";
        $conn->query($sql);
    }

    else if ((int) $row['conversions'] == 0 && (int) $row['clicks'] > 0) {
        $sql = "INSERT IGNORE INTO wilma_bing.conversion_rates (mag_sku, campaign_name, clicks , conversions , campaign_id , adgroup_id , all_time , bucket, updated) VALUES (" . $row['item_id'] . ", '" . $row['campaign_name'] . "', " . $row['clicks'] . ", " . $row['conversions'] . " , '" . $row['campaign_id'] . "' ,'" . $row['adgroup_id'] . "' , ROUND(" . 1 / $row['clicks'] . " ,3) , 'clicked' , 1) ON DUPLICATE KEY UPDATE "
            ." updated = IF( clicks <> VALUES(clicks) , updated + 1 , updated) , clicks = VALUES(clicks) , bucket = 'clicked' ";
        //echo($sql);
        $conn->query($sql);
    }

}



//echo "All Time Completed.\r\n\r\n";




// 7 Day Converted


$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_7  WHERE conversions > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.7_day = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");

// 7 Day Clicked

$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_7  WHERE conversions = 0 AND clicks > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.7_day = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");




//echo "7 Day Completed.\r\n\r\n";


// 30 Day Converted


$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_30  WHERE conversions > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.30_day = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");

// 30 Day Clicked

$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_30  WHERE conversions = 0 AND clicks > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.30_day = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");


//echo "30 Day Completed.\r\n\r\n";

// 90 Day Converted

$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_90  WHERE conversions > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.90_day = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");

// 90 Day Clicked

$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_90  WHERE conversions = 0 AND clicks > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.90_day = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");


//echo "90 Day Completed.\r\n\r\n";


// 180 Day Converted

$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_180  WHERE conversions > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.180_day = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");

// 180 Day Clicked

$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_180  WHERE conversions = 0 AND clicks > 0 GROUP BY item_id , campaign_id) b "
    ."SET a.180_day = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");


//echo "180 Day Completed.\r\n\r\n";


// 365 Day Converted

$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_365  WHERE conversions > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.365_day = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");

// 365 Day Clicked

$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_365  WHERE conversions = 0 AND clicks > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.365_day = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");





//echo "365 Day Completed.\r\n\r\n";


// Real Time Day Converted

$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_real_time  WHERE conversions > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.real_time = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");

// Real Time  Clicked

$conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
    ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_real_time  WHERE conversions = 0 AND clicks > 0  GROUP BY item_id , campaign_id) b "
    ."SET a.real_time = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");


//echo "Real Time Completed.\r\n\r\n";




$results = $conn->query("SELECT * FROM wilma_bing.conversion_rates WHERE updated = 1 ");

while ($row = $results->fetch_assoc()) {
    $day7 = $row['7_day'];
    $day30 = $row['30_day'];
    $day90 = $row['90_day'];
    $day180 = $row['180_day'];
    $day365 = $row['365_day'];
    $alltime = $row['all_time'];

    $weightingday7 = 0.40;
    $weightingday30 = 0.30;
    $weightingday90 = 0.10;
    $weightingday180 = 0.10;
    $weightingalltime = 0.10;

    if ($day7 == NULL) $weightingday30 = $weightingday30 + $weightingday7;
    if ($day30 == NULL) $weightingday90 = $weightingday90 + $weightingday30;
    if ($day90 == NULL) $weightingday180 = $weightingday180 + $weightingday90;
    if ($day180 == NULL) $weightingalltime = $weightingalltime + $weightingday180;



    if ($row['bucket'] == 'clicked') {
        $weightingday7 = $weightingday30 = $weightingday90 = $weightingday180 = 0.00;
        $weightingalltime = 1.00;
    }


    $weighted_conversion_rate = ($day7 * $weightingday7) + ($day30 * $weightingday30) + ($day90 * $weightingday90) +
        ($day180 * $weightingday180) + ($alltime * $weightingalltime);

    if ($row['bucket'] == 'converted'){

        $conn->query("UPDATE wilma_bing.conversion_rates SET weighted = ROUND(" . $weighted_conversion_rate . " , 3) WHERE mag_sku = " . $row['mag_sku'] . " AND campaign_id='" . $row['campaign_id'] . "'   ;");

    }

    if ($row['bucket'] == 'clicked'){

        $conn->query("UPDATE wilma_bing.conversion_rates SET estimated = ROUND(" . $weighted_conversion_rate . " , 3) WHERE mag_sku = " . $row['mag_sku'] . " AND campaign_id='" . $row['campaign_id'] . "' ; ");

    }

}





//echo "Start Sync Camapiagn Tables.\r\n\r\n";

// INSERT Items Not In campaign tables and sync capaign tables

//MDMB

$conn->query("INSERT IGNORE INTO wilma_bing.cm_mdmb  (item_id , campaign_id , adgroup_id , bucket , adgroup_name , impressions , clicks , conversions , cost , average_cpc)
SELECT sku , campaign_id , adgroup_id , current_bucket , adgroup_name , total_impressions , total_clicks , total_conversions , total_cost , average_cpc 
FROM wilma_bing.bid_summary WHERE campaign_id = 398174306 ON DUPLICATE KEY UPDATE wilma_bing.cm_mdmb.bucket = wilma_bing.bid_summary.current_bucket , 
wilma_bing.cm_mdmb.impressions = wilma_bing.bid_summary.total_impressions , wilma_bing.cm_mdmb.clicks = wilma_bing.bid_summary.total_clicks , 
wilma_bing.cm_mdmb.conversions = wilma_bing.bid_summary.total_conversions , wilma_bing.cm_mdmb.cost = wilma_bing.bid_summary.total_cost , 
wilma_bing.cm_mdmb.average_cpc = wilma_bing.bid_summary.average_cpc ;");



//MDPART

$conn->query("INSERT IGNORE INTO wilma_bing.cm_mdpart  (item_id , campaign_id , adgroup_id , bucket , adgroup_name , impressions , clicks , conversions , cost , average_cpc)
SELECT sku , campaign_id , adgroup_id , current_bucket , adgroup_name , total_impressions , total_clicks , total_conversions , total_cost , average_cpc 
FROM wilma_bing.bid_summary WHERE campaign_id = 398174268 ON DUPLICATE KEY UPDATE wilma_bing.cm_mdpart.bucket = wilma_bing.bid_summary.current_bucket , 
wilma_bing.cm_mdpart.impressions = wilma_bing.bid_summary.total_impressions , wilma_bing.cm_mdpart.clicks = wilma_bing.bid_summary.total_clicks , 
wilma_bing.cm_mdpart.conversions = wilma_bing.bid_summary.total_conversions , wilma_bing.cm_mdpart.cost = wilma_bing.bid_summary.total_cost , 
wilma_bing.cm_mdpart.average_cpc = wilma_bing.bid_summary.average_cpc 
 ;");



//MedicalPLA

$conn->query("INSERT IGNORE INTO wilma_bing.cm_mdpla  (item_id , campaign_id , adgroup_id , bucket , adgroup_name , impressions , clicks , conversions , cost , average_cpc)
SELECT sku , campaign_id , adgroup_id , current_bucket , adgroup_name , total_impressions , total_clicks , total_conversions , total_cost , average_cpc 
FROM wilma_bing.bid_summary WHERE campaign_id = 398174273 ON DUPLICATE KEY UPDATE wilma_bing.cm_mdpla.bucket = wilma_bing.bid_summary.current_bucket , 
wilma_bing.cm_mdpla.impressions = wilma_bing.bid_summary.total_impressions , wilma_bing.cm_mdpla.clicks = wilma_bing.bid_summary.total_clicks , 
wilma_bing.cm_mdpla.conversions = wilma_bing.bid_summary.total_conversions , wilma_bing.cm_mdpla.cost = wilma_bing.bid_summary.total_cost , 
wilma_bing.cm_mdpla.average_cpc = wilma_bing.bid_summary.average_cpc ;");

//MDTAB

$conn->query("INSERT IGNORE INTO wilma_bing.cm_mdtab  (item_id , campaign_id , adgroup_id , bucket , adgroup_name , impressions , clicks , conversions , cost , average_cpc)
SELECT sku , campaign_id , adgroup_id , current_bucket , adgroup_name , total_impressions , total_clicks , total_conversions , total_cost , average_cpc 
FROM wilma_bing.bid_summary WHERE campaign_id = 398174277 ON DUPLICATE KEY UPDATE wilma_bing.cm_mdtab.bucket = wilma_bing.bid_summary.current_bucket , 
wilma_bing.cm_mdtab.impressions = wilma_bing.bid_summary.total_impressions , wilma_bing.cm_mdtab.clicks = wilma_bing.bid_summary.total_clicks , 
wilma_bing.cm_mdtab.conversions = wilma_bing.bid_summary.total_conversions , wilma_bing.cm_mdtab.cost = wilma_bing.bid_summary.total_cost , 
wilma_bing.cm_mdtab.average_cpc = wilma_bing.bid_summary.average_cpc ;");


//echo "Prep Conversion rates.\r\n\r\n";

//Prep Conversion rates 

// MDPART

//Clicked

$conn->query("UPDATE wilma_bing.cm_mdpart a , wilma_bing.conversion_rates b SET a.estimated_conversion_rate = b.estimated , a.updated = 1 , b.updated = 0 "
    ."WHERE a.item_id= b.mag_sku AND b.campaign_id = '398174268' AND b.bucket = 'clicked' AND b.updated = 1   ; ");

//Converted

$conn->query("UPDATE wilma_bing.cm_mdpart a , wilma_bing.conversion_rates b SET a.weighted_conversion_rate = b.weighted , a.updated = 1 , b.updated = 0 "
    ."WHERE a.item_id= b.mag_sku AND b.campaign_id = '398174268' AND b.bucket = 'converted' AND b.updated = 1   ; ");

//MDPLA

//Clicked

$conn->query("UPDATE wilma_bing.cm_mdpla a , wilma_bing.conversion_rates b SET a.estimated_conversion_rate = b.estimated , a.updated = 1 , b.updated = 0 "
    ."WHERE a.item_id= b.mag_sku AND b.campaign_id = '398174273' AND b.bucket = 'clicked' AND b.updated = 1   ; ");

//Converted

$conn->query("UPDATE wilma_bing.cm_mdpla a , wilma_bing.conversion_rates b SET a.weighted_conversion_rate = b.weighted , a.updated = 1 , b.updated = 0 "
    ."WHERE a.item_id= b.mag_sku AND b.campaign_id = '398174273' AND b.bucket = 'converted' AND b.updated = 1   ; ");


//MDMB

//Clicked

$conn->query("UPDATE wilma_bing.cm_mdmb a , wilma_bing.conversion_rates b SET a.estimated_conversion_rate = b.estimated , a.updated = 1 , b.updated = 0 "
    ."WHERE a.item_id= b.mag_sku AND b.campaign_id = '398174306' AND b.bucket = 'clicked' AND b.updated = 1   ; ");

//Converted

$conn->query("UPDATE wilma_bing.cm_mdmb a , wilma_bing.conversion_rates b SET a.weighted_conversion_rate = b.weighted , a.updated = 1 , b.updated = 0 "
    ."WHERE a.item_id= b.mag_sku AND b.campaign_id = '398174306' AND b.bucket = 'converted' AND b.updated = 1   ; ");

//MDTAB

//Clicked

$conn->query("UPDATE wilma_bing.cm_mdtab a , wilma_bing.conversion_rates b SET a.estimated_conversion_rate = b.estimated , a.updated = 1 , b.updated = 0 "
    ."WHERE a.item_id= b.mag_sku AND b.campaign_id = '398174277' AND b.bucket = 'clicked' AND b.updated = 1   ; ");

//Converted

$conn->query("UPDATE wilma_bing.cm_mdtab a , wilma_bing.conversion_rates b SET a.weighted_conversion_rate = b.weighted , a.updated = 1 , b.updated = 0 "
    ."WHERE a.item_id= b.mag_sku AND b.campaign_id = '398174277' AND b.bucket = 'converted' AND b.updated = 1   ; ");




//echo "Prep Conversion rates Completed.\r\n\r\n";




//echo "Prep Bid Adjustments.\r\n\r\n";

//Prep Bid Adjustments

//MDMB first

$conn->query("UPDATE wilma_bing.cm_mdmb c , (SELECT a.sku , a.last_bid , b.average_cpc FROM wilma_bing.bid_summary a INNER JOIN wilma_bing.cm_mdmb b ON a.sku = b.item_id AND a.campaign_id = b.campaign_id "
    ."WHERE b.bucket = 'converted' AND b.updated = 1 AND a.last_bid > b.average_cpc) d SET c.bid_adjustment = ROUND(d.average_cpc / d.last_bid , 3) WHERE c.item_id = d.sku ");


//MDPART


$conn->query("UPDATE wilma_bing.cm_mdpart c , (SELECT a.sku , a.last_bid , b.average_cpc FROM wilma_bing.bid_summary a INNER JOIN wilma_bing.cm_mdpart b ON a.sku = b.item_id AND a.campaign_id = b.campaign_id "
    ."WHERE b.bucket = 'converted' AND b.updated = 1 AND a.last_bid > b.average_cpc) d SET c.bid_adjustment = ROUND(d.average_cpc / d.last_bid , 3) WHERE c.item_id = d.sku ");



//MedicalPLA


$conn->query("UPDATE wilma_bing.cm_mdpla c , (SELECT a.sku , a.last_bid , b.average_cpc FROM wilma_bing.bid_summary a INNER JOIN wilma_bing.cm_mdpla b ON a.sku = b.item_id AND a.campaign_id = b.campaign_id "
    ."WHERE b.bucket = 'converted' AND b.updated = 1 AND a.last_bid > b.average_cpc) d SET c.bid_adjustment = ROUND(d.average_cpc / d.last_bid , 3) WHERE c.item_id = d.sku ");



//MDTAB

$conn->query("UPDATE wilma_bing.cm_mdtab c , (SELECT a.sku , a.last_bid , b.average_cpc FROM wilma_bing.bid_summary a INNER JOIN wilma_bing.cm_mdtab b ON a.sku = b.item_id AND a.campaign_id = b.campaign_id "
    ."WHERE b.bucket = 'converted' AND b.updated = 1 AND a.last_bid > b.average_cpc) d SET c.bid_adjustment = ROUND(d.average_cpc / d.last_bid , 3) WHERE c.item_id = d.sku ");

$executionEndTime = microtime(true);

$seconds = $executionEndTime - $executionStartTime;
//echo "This script took $seconds to execute.";

?>
