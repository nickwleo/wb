<?php
/************************ DATABASE CONNECTION START HERE   ****************************/

error_reporting(E_ALL ^ E_DEPRECATED);

include  __DIR__ . '/WilmaConfig.php';

// Create connection
$conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);

if (!$conn) {
    die("Could not connect: " . mysqli_error());
}

mysqli_select_db($conn, "wilma_bing");

/************************ DATABASE CONNECTION END HERE  ****************************/
$cpc_buffer = 0.20 ;

$campaigns = array("0" => "cm_mdpla" ,"1" => "cm_mdpart" , "2" => "cm_mdmb" , "3" => "cm_mdtab" );
foreach ($campaigns as $campaign) {
    $table = $campaign;
    $short_name = '';

    If($table == 'cm_mdpla'){$short_name = 'MDPLA' ;}

    If($table == 'cm_mdpart'){$short_name = 'MDPART' ;}

    If($table == 'cm_mdtab'){$short_name = 'MDTAB' ;}

    If($table == 'cm_mdmb'){$short_name = 'MDMB' ;}

    print_r($table);
    print_r($short_name);

    //Ensures that the necessary adgroup and campaign names are populated.
    $conn->query('UPDATE products_on_bing, adgroups SET products_on_bing.campaign_name = adgroups.campaign_name, products_on_bing.adgroup_name = adgroups.name WHERE products_on_bing.adgroup_id = adgroups.bing_id');

    //Medical PLA Bids

    //Set Stock Status For Campaign

    $conn->query("UPDATE `wilma_bing`.".$table ." a , `wilma_bing`.master_product b SET a.stock_status = 1 , a.updated = 1 WHERE a.item_id = b.mag_sku AND b.qty > 0 AND a.stock_status = 0");
    $conn->query("UPDATE `wilma_bing`.".$table ." a , `wilma_bing`.master_product b SET a.stock_status = 0 , a.updated = 1 WHERE a.item_id = b.mag_sku AND b.qty = 0 AND a.stock_status = 1");
    //$conn->query("UPDATE `wilma_bing`.".$table ." a , `wilma_bing`.master_product b SET a.enabled = 2 , a.updated = 1 WHERE a.item_id = b.mag_sku AND a.enabled = 1");
    //$conn->query("UPDATE `wilma_bing`.".$table ." a , `wilma_bing`.master_product b SET a.enabled = 1 , a.updated = 1 WHERE a.item_id = b.mag_sku AND a.enabled = 2");

    $conn->query("UPDATE `wilma_bing`.".$table ." SET new_bid = 0.01 WHERE (stock_status = 0 or enabled = 2) AND updated = 1");

    //Begin Bidding Routine

    $check_updated = $conn->query("SELECT item_id , campaign_id , adgroup_id FROM `wilma_bing`.".$table ." WHERE updated = 1");

    if (mysqli_num_rows($check_updated) > 0){
        // Clicked Bidding
        $clicked_products = $conn->query("UPDATE `wilma_bing`.".$table ."  d ,(SELECT c.mag_sku , ROUND(c.margin * c.estimated_conversion_rate , 2) AS maxcpc FROM
		(SELECT a.mag_sku , (a.margin * .10) AS margin , b.estimated_conversion_rate  FROM `wilma_bing`.master_product a INNER JOIN `wilma_bing`.".$table ." b ON a.mag_sku = b.item_id
		WHERE b.bucket = 'clicked'  AND b.estimated_conversion_rate IS NOT NULL AND b.stock_status = 1) c)e SET d.new_bid = e.maxcpc WHERE d.item_id = e.mag_sku AND d.updated = 1 AND d.enabled = 1");

        // Converted Bidding
        $converted = $conn->query("SELECT a.mag_sku , a.margin , a.ordercount , a.avg_qty_sold , b.weighted_conversion_rate , b.bid_adjustment FROM `wilma_bing`.master_product a INNER JOIN `wilma_bing`.".$table ." b ON a.mag_sku = b.item_id
		WHERE b.bucket = 'converted'  AND b.weighted_conversion_rate IS NOT NULL AND b.stock_status = 1 AND b.updated = 1 AND b.enabled = 1 ");

        while ($bids = mysqli_fetch_assoc($converted)){
            $sku = $bids['mag_sku'];
            $margin = $bids['margin'];
            $order_count = $bids['ordercount'];
            $avg_qty = $bids['avg_qty_sold'];
            $weighted_conv = $bids['weighted_conversion_rate'];
            $bid_adj = $bids['bid_adjustment'];

            if((!empty($bid_adj)) AND ($order_count >= 3)  ){
                $conn->query("UPDATE `wilma_bing`.".$table ." SET new_bid = ROUND((((".$margin ." * ".$avg_qty .") *.10) * ".$weighted_conv .") * $bid_adj , 2)  WHERE item_id = ".$sku." ; ");
            }else if((!empty($bid_adj)) AND ($order_count <= 2)  ) {
                $conn->query("UPDATE `wilma_bing`.".$table ." SET new_bid = ROUND(((".$margin ." *.10) * ". $weighted_conv .") * $bid_adj , 2)  WHERE item_id = ".$sku." ; ");
            }else if((empty($bid_adj)) AND ($order_count >= 3)  ){
                $conn->query("UPDATE `wilma_bing`.".$table ." SET new_bid = ROUND(((".$margin ." * ".$avg_qty .") *.10) * ".$weighted_conv." , 2)  WHERE item_id = ".$sku." ; ");
            }else if((empty($bid_adj)) AND ($order_count <= 2)  ) {
                $conn->query("UPDATE `wilma_bing`.".$table ." SET new_bid = ROUND((".$margin ." *.10) * ".$weighted_conv ." , 2)  WHERE item_id = ".$sku." ; ");
            }else if((empty($bid_adj)) AND (empty($order_count))  ) {
                $conn->query("UPDATE `wilma_bing`.".$table ." SET new_bid = ROUND((".$margin ." *.10) * ".$weighted_conv ." , 2)  WHERE item_id = ".$sku." ; ");
            }
        }

        // Viewed
        $viewed = $conn->query("UPDATE `wilma_bing`.".$table ."  d ,(SELECT a.mag_sku , a.margin  FROM `wilma_bing`.master_product a INNER JOIN `wilma_bing`.".$table ." b ON a.mag_sku = b.item_id "
            ."WHERE b.bucket = 'viewed' AND b.stock_status = 1) c SET d.new_bid = ROUND(c.margin * .70 , 2) WHERE d.item_id = c.mag_sku AND d.updated = 1 AND d.enabled = 1   ");

        // Not Viewed
        $not_viewed = $conn->query("UPDATE `wilma_bing`.".$table ."  d , (SELECT a.mag_sku , a.margin  FROM `wilma_bing`.master_product a INNER JOIN `wilma_bing`.".$table ." b ON a.mag_sku = b.item_id "
            ."WHERE b.bucket = 'not_viewed' AND b.stock_status = 1) c SET d.new_bid = ROUND(c.margin * .70 , 2) WHERE d.item_id = c.mag_sku and d.updated = 1 AND d.enabled = 1 ");

        //Clean UP
        $conn->query("UPDATE `wilma_bing`.".$table ." SET new_bid = 0.01 WHERE new_bid = 0.00 OR new_bid < 0 ");
        $conn->query("UPDATE `wilma_bing`.".$table ." SET new_bid = ".$cpc_buffer." WHERE new_bid > ".$cpc_buffer."  ");

        // Send bids to bid table
        $conn->query("INSERT INTO `wilma_bing`.bids (mag_sku , campaign_name , bid) SELECT a.item_id , '".$short_name."', a.new_bid FROM `wilma_bing`.".$table ." a INNER JOIN `wilma_bing`.bid_summary b "
            ." ON a.item_id = b.sku AND a.campaign_id = b.campaign_id AND a.adgroup_id = b.adgroup_id WHERE a.updated = 1  AND a.new_bid <> b.last_bid   ");

        $bid_update = "UPDATE `wilma_bing`.".$table ." SET updated = CASE item_id ";
        $skucollection = '';
        while ($data = mysqli_fetch_assoc($check_updated)) {
            $magSkus = $data['item_id'];

            $bid_update .= " WHEN {$magSkus} THEN '0' ";
            $skucollection .= " {$magSkus},";
        }
        $skucollection = rtrim($skucollection,',');
        $bid_update .= " END ";
        $bid_update .= " WHERE item_id IN (".rtrim($skucollection,',').") AND updated = 1";

        $conn->query($bid_update);

    }else echo "Nothing to process in table $table ";
}
