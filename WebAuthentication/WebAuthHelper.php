<?php

namespace Microsoft\BingAds;

use Microsoft\BingAds\Auth\ApiEnvironment;

/** 
 * Defines global settings that you can use for testing your application.
 * Your production implementation may vary, and you should always store sensitive information securely.
 */
final class WebAuthHelper {

    const DeveloperToken = 'BBD37VB98'; // For sandbox use BBD37VB98
    const ApiEnvironment = ApiEnvironment::Sandbox;
    const ClientId = '2594030a-af26-4b15-bb30-709aeeb9429c';
    const ClientSecret = 'ieiiWHG78!%hqiMYWO475-{';
    const RedirectUri = 'https://login.live.com/oauth20_desktop.srf';
}
