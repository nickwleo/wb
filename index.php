<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 18-10-12
 * Time: 1:27 PM
 */

require_once __DIR__ . "/vendor/autoload.php";

use Microsoft\BingAds\V12\CampaignManagement\GetCampaignsByAccountIdRequest;

//AuthHelper::AuthenticateWithOAuth();

function GetCampaignsByAccountId(
    $accountId,
    $campaignType)
{
    $GLOBALS['CampaignManagementProxy']->SetAuthorizationData($GLOBALS['AuthorizationData']);
    $GLOBALS['Proxy'] = $GLOBALS['CampaignManagementProxy'];

    $request = new GetCampaignsByAccountIdRequest();

    $request->AccountId = $accountId;
    $request->CampaignType = $campaignType;

    return $GLOBALS['CampaignManagementProxy']->GetService()->GetCampaignsByAccountId($request);
}

?>


