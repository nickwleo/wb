<?php

namespace Microsoft\BingAds\Samples\V13;

// For more information about installing and using the Bing Ads PHP SDK,
// see https://go.microsoft.com/fwlink/?linkid=838593.

require_once __DIR__ . "/../vendor/autoload.php";

include __DIR__ . "/AuthHelper.php";
include "CampaignManagementExampleHelper.php";

use SoapVar;
use SoapFault;
use Exception;

// Specify the Microsoft\BingAds\V13\CampaignManagement classes that will be used.
use Microsoft\BingAds\V13\CampaignManagement\Campaign;
use Microsoft\BingAds\V13\CampaignManagement\BudgetLimitType;
use Microsoft\BingAds\V13\CampaignManagement\BMCStore;
use Microsoft\BingAds\V13\CampaignManagement\ShoppingSetting;
use Microsoft\BingAds\V13\CampaignManagement\CampaignType;
use Microsoft\BingAds\V13\CampaignManagement\AdGroup;
use Microsoft\BingAds\V13\CampaignManagement\Date;
use Microsoft\BingAds\V13\CampaignManagement\ProductAd;
use Microsoft\BingAds\V13\CampaignManagement\CampaignCriterionType;
use Microsoft\BingAds\V13\CampaignManagement\ProductScope;
use Microsoft\BingAds\V13\CampaignManagement\BiddableCampaignCriterion;
use Microsoft\BingAds\V13\CampaignManagement\ProductCondition;
use Microsoft\BingAds\V13\CampaignManagement\ProductPartitionType;
use Microsoft\BingAds\V13\CampaignManagement\AdGroupCriterionType;
use Microsoft\BingAds\V13\CampaignManagement\BiddableAdGroupCriterion;
use Microsoft\BingAds\V13\CampaignManagement\ItemAction;
use Microsoft\BingAds\V13\CampaignManagement\AdGroupCriterionAction;
use Microsoft\BingAds\V13\CampaignManagement\NegativeAdGroupCriterion;
use Microsoft\BingAds\V13\CampaignManagement\ProductPartition;
use Microsoft\BingAds\V13\CampaignManagement\FixedBid;
use Microsoft\BingAds\V13\CampaignManagement\Bid;
use Microsoft\BingAds\V13\CampaignManagement\BatchErrorCollection;

// Specify the Microsoft\BingAds\Auth classes that will be used.
use Microsoft\BingAds\Auth\ServiceClient;
use Microsoft\BingAds\Auth\ServiceClientType;

// Specify the Microsoft\BingAds\Samples classes that will be used.
use Microsoft\BingAds\Samples\V13\AuthHelper;
use Microsoft\BingAds\Samples\V13\CampaignManagementExampleHelper;

$GLOBALS['AuthorizationData'] = null;
$GLOBALS['Proxy'] = null;
$GLOBALS['CampaignManagementProxy'] = null;

// Disable WSDL caching.

ini_set("soap.wsdl_cache_enabled", "0");
ini_set("soap.wsdl_cache_ttl", "0");

$PartitionActions = array(); // AdGroupCriterionAction array
$ReferenceId = -1;
$ids = null;

try
{
    // Authenticate for Bing Ads services with a Microsoft Account.

    AuthHelper::Authenticate();

    $GLOBALS['CampaignManagementProxy'] = new ServiceClient(
        ServiceClientType::CampaignManagementVersion13,
        $GLOBALS['AuthorizationData'],
        AuthHelper::GetApiEnvironment());

    $stores= CampaignManagementExampleHelper::GetBMCStoresByCustomerId(false)->BMCStores->BMCStore;

    if (!isset($stores))
    {
        printf("CustomerId %d does not have any registered BMC stores.\n\n", $CustomerId);
        return;
    }

    // Create a Bing Shopping campaign using the ID of the first store in the list.
    $settings = array();

    $shoppingSetting = new ShoppingSetting();
    $shoppingSetting->Priority = 0;
    $shoppingSetting->SalesCountryCode = "US";
    $shoppingSetting->StoreId = $stores[0]->Id;

    $encodedSetting = new SoapVar(
        $shoppingSetting,
        SOAP_ENC_OBJECT,
        'ShoppingSetting',
        $GLOBALS['CampaignManagementProxy']->GetNamespace());
    $settings[] = $encodedSetting;

    require __DIR__ . '/../WilmaConfig.php';

    $conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);

    $query = "SELECT * FROM bids INNER JOIN products_on_bing ON bids.mag_sku = products_on_bing.mag_sku AND bids.campaign_name = products_on_bing.campaign_name WHERE sent = 0";

    $bidsToSend = $conn->query($query);

    while ($bid = mysqli_fetch_array($bidsToSend)) {
        UpdateProductBid(
            $GLOBALS['AuthorizationData']->AccountId,
            $PartitionActions,
            $bid['adgroup_id'],
            $bid['bid'],
            $bid['bing_id']);
    }
}
catch (SoapFault $e)
{
    print "\nLast SOAP request/response:\n";
    printf("Fault Code: %s\nFault String: %s\n", $e->faultcode, $e->faultstring);
    print $GLOBALS['Proxy']->GetWsdl() . "\n";
    print $GLOBALS['Proxy']->GetService()->__getLastRequest()."\n";
    print $GLOBALS['Proxy']->GetService()->__getLastResponse()."\n";

    if (isset($e->detail->AdApiFaultDetail))
    {
        CampaignManagementExampleHelper::OutputAdApiFaultDetail($e->detail->AdApiFaultDetail);

    }
    elseif (isset($e->detail->ApiFaultDetail))
    {
        CampaignManagementExampleHelper::OutputApiFaultDetail($e->detail->ApiFaultDetail);
    }
    elseif (isset($e->detail->EditorialApiFaultDetail))
    {
        CampaignManagementExampleHelper::OutputEditorialApiFaultDetail($e->detail->EditorialApiFaultDetail);
    }
}
catch (Exception $e)
{
    // Ignore fault exceptions that we already caught.
    if ($e->getPrevious())
    { ; }
    else
    {
        print $e->getCode()." ".$e->getMessage()."\n\n";
        print $e->getTraceAsString()."\n\n";
    }
}

function UpdateProductBid($accountId, &$actions, $adGroupId, $wilmaBidAmount, $productBingId)
{
    // Update the bid of the root node that we just added.

    $updatedRoot = new BiddableAdGroupCriterion();
    $updatedRoot->Id = $productBingId;
    $updatedRoot->AdGroupId = $adGroupId;
    $updatedRoot->CriterionBid = GetFixedBid($wilmaBidAmount);//0.40

    $encodedUpdateRoot = new SoapVar(
        $updatedRoot,
        SOAP_ENC_OBJECT,
        'BiddableAdGroupCriterion',
        $GLOBALS['CampaignManagementProxy']->GetNamespace());

    $actions = array();  // clear

    AddPartitionAction($encodedUpdateRoot, ItemAction::Update, $actions);

    printf("Updating the bid for the tree root node...\n\n");

    $applyPartitionActionsResponse = CampaignManagementExampleHelper::ApplyProductPartitionActions($actions);

    printf("Updated the bid for the tree root node\n\n");
}

// Get the root criterion node.

function GetRootNode($adGroupCriterions)
{
    $rootNode = null;

    foreach ($adGroupCriterions->AdGroupCriterion as $adGroupCriterion)
    {
        if (empty($adGroupCriterion->Criterion->ParentCriterionId))
        {
            $rootNode = $adGroupCriterion;
            break;
        }
    }

    return $rootNode;
}

// Gets a fixed bid object with the specified bid amount.

function GetFixedBid($bidAmount)
{
    $fixedBid = new FixedBid();
    $fixedBid->Amount = $bidAmount;

    $encodedFixedBid = new SoapVar(
        $fixedBid,
        SOAP_ENC_OBJECT,
        'FixedBid',
        $GLOBALS['CampaignManagementProxy']->GetNamespace());

    return $encodedFixedBid;
}

// Adds a criterion action to the list of actions.

function AddPartitionAction($criterion, $itemAction, &$actions)
{
    $partitionAction = new AdGroupCriterionAction();
    $partitionAction->Action = $itemAction;
    $partitionAction->AdGroupCriterion = $criterion;

    $actions[] = $partitionAction;
}
