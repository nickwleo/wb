<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 28/05/18
 * Time: 1:59 PM
 */

include  __DIR__ . '/../WilmaConfig.php';

// Create connection
$conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "\r\nConnected successfully\r\n";

//MDMB first

$results = $conn->query("SELECT * FROM wilma_bing.bid_summary a INNER JOIN wilma_bing.cm_mdmb b ON a.sku = b.item_id AND a.campaign_id = b.campaign_id AND a.adgroup_id = b.adgroup_id  WHERE b.bucket = 'converted' AND b.updated = 1;");

while ($row = $results->fetch_assoc()) {

    if ($row['last_bid'] > $row['average_cpc']) {

        $bid_adjustment = (float) $row['average_cpc'] / $row['last_bid'];

        $update_query = "UPDATE wilma_bing.cm_mdmb SET bid_adjustment = '" . $bid_adjustment . "' WHERE item_id=" . $row['item_id']. " AND campaign_id = '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "' ";

        $conn->query($update_query);

    }

}

 $conn->query("UPDATE wilma_bing.conversion_rates SET updated = 0 WHERE mag_sku = " . $row['mag_sku']." AND campaign_id =  '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "'  ; ");

//MDPART



$results = $conn->query("SELECT * FROM wilma_bing.bid_summary a INNER JOIN wilma_bing.cm_mdpart b ON a.sku = b.item_id AND a.campaign_id = b.campaign_id AND a.adgroup_id = b.adgroup_id  WHERE b.bucket = 'converted' AND b.updated = 1");

while ($row = $results->fetch_assoc()) {

    if ($row['last_bid'] > $row['average_cpc']) {

        $bid_adjustment = (float) $row['average_cpc'] / $row['last_bid'];

        $update_query = "UPDATE wilma_bing.cm_mdpart SET bid_adjustment = '" . $bid_adjustment . "' WHERE item_id=" . $row['item_id'] ." AND campaign_id = '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "'";

        $conn->query($update_query);

    }

}

//MedicalPLA



$results = $conn->query("SELECT * FROM wilma_bing.bid_summary a INNER JOIN wilma_bing.cm_mdpla b ON a.sku = b.item_id AND a.campaign_id = b.campaign_id AND a.adgroup_id = b.adgroup_id  WHERE b.bucket = 'converted' AND b.updated = 1");

while ($row = $results->fetch_assoc()) {

    if ($row['last_bid'] > $row['average_cpc']) {

        $bid_adjustment = (float) $row['average_cpc'] / $row['last_bid'];

        $update_query = "UPDATE wilma_bing.cm_mdpla SET bid_adjustment = '" . $bid_adjustment . "' WHERE item_id=" . $row['item_id'] ." AND campaign_id = '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "'";

        $conn->query($update_query);

    }

}

//MDTAB

$results = $conn->query("SELECT * FROM wilma_bing.bid_summary a INNER JOIN wilma_bing.cm_mdtab b ON a.sku = b.item_id AND a.campaign_id = b.campaign_id AND a.adgroup_id = b.adgroup_id  WHERE b.bucket = 'converted' AND b.updated = 1");

while ($row = $results->fetch_assoc()) {

    if ($row['last_bid'] > $row['average_cpc']) {

        $bid_adjustment = (float) $row['average_cpc'] / $row['last_bid'];

        $update_query = "UPDATE wilma_bing.cm_mdtab SET bid_adjustment = '" . $bid_adjustment . "' WHERE item_id=" . $row['item_id']." AND campaign_id = '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "'";

        $conn->query($update_query);

    }

}
