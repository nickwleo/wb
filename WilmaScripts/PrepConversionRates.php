<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 21/05/18
 * Time: 12:08 AM
 */

include  __DIR__ . '/../WilmaConfig.php';

// Create connection
$conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "\r\nConnected successfully\r\n\r\n";

$results = $conn->query("SELECT * FROM wilma_bing.conversion_rates WHERE updated = 1");

//$count = 0;

$resultCount = mysqli_num_rows($results);

while ($row = $results->fetch_assoc()) {


    if ($row['bucket'] == 'clicked' && $row['campaign_id'] == 370648447 ) $update_query = "UPDATE wilma_bing.cm_mdpart SET estimated_conversion_rate='" . $row['estimated'] . "' , updated = 1 WHERE item_id=" . $row['mag_sku']." AND campaign_id =  '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "' ; ";
    else if ($row['bucket'] == 'converted' && $row['campaign_id'] == 370648447) $update_query = "UPDATE wilma_bing.cm_mdpart SET weighted_conversion_rate='" . $row['weighted'] . "' , updated = 1 WHERE item_id=" . $row['mag_sku']." AND campaign_id =  '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "'; ";

    if ($row['bucket'] == 'clicked' && $row['campaign_id'] == 357968167) $update_query = "UPDATE wilma_bing.cm_mdpla SET estimated_conversion_rate='" . $row['estimated'] . "' , updated = 1 WHERE item_id=" . $row['mag_sku']." AND campaign_id =  '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "' ; ";
    else if ($row['bucket'] == 'converted' && $row['campaign_id'] == 357968167) $update_query = "UPDATE wilma_bing.cm_mdpla SET weighted_conversion_rate='" . $row['weighted'] . "' , updated = 1 WHERE item_id=" . $row['mag_sku']." AND campaign_id =  '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "' ; ";

    if ($row['bucket'] == 'clicked' && $row['campaign_id'] == 370648567) $update_query = "UPDATE wilma_bing.cm_mdmb SET estimated_conversion_rate='" . $row['estimated'] . "' , updated = 1 WHERE item_id=" . $row['mag_sku']." AND campaign_id =  '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "' ; ";
    else if ($row['bucket'] == 'converted' && $row['campaign_id'] == 370648567) $update_query = "UPDATE wilma_bing.cm_mdmb SET weighted_conversion_rate='" . $row['weighted'] . "' , updated = 1 WHERE item_id=" . $row['mag_sku']." AND campaign_id =  '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "'  ";

    if ($row['bucket'] == 'clicked' && $row['campaign_id'] == 728828627) $update_query = "UPDATE wilma_bing.cm_mdtab SET estimated_conversion_rate='" . $row['estimated'] . "' , updated = 1 WHERE item_id=" . $row['mag_sku']." AND campaign_id =  '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "' ; ";
    else if ($row['bucket'] == 'converted' && $row['campaign_id'] == 728828627) $update_query = "UPDATE wilma_bing.cm_mdtab SET weighted_conversion_rate='" . $row['weighted'] . "' , updated = 1 WHERE item_id=" . $row['mag_sku']." AND campaign_id =  '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "' ; ";

    $conn->query($update_query);
    
    
    $conn->query("UPDATE wilma_bing.conversion_rates SET updated = 0 WHERE mag_sku = " . $row['mag_sku']." AND campaign_id =  '" . $row['campaign_id'] . "' AND adgroup_id = '" . $row['adgroup_id'] . "'  ; ");
    
    
    //print_r($update_query);

    //$count++;

    //echo (chr(8) . chr(8) . chr(8) . chr(8) . chr(8) . chr(8) . chr(8));

    //echo (round($count / $resultCount * 100,2) . "%");

}
    




//include('PrepBidAdjustments.php');

