<?php

namespace Microsoft\BingAds\Samples\V13;

require_once __DIR__ . "/../vendor/autoload.php";

include __DIR__ . "/AuthHelper.php";
include "CampaignManagementExampleHelper.php";
include  __DIR__ . '/../WilmaConfig.php';

// Specify the Microsoft\BingAds\V12\CampaignManagement classes that will be used.
use Microsoft\BingAds\V12\CampaignManagement\Campaign;
use Microsoft\BingAds\V12\CampaignManagement\BudgetLimitType;
use Microsoft\BingAds\V12\CampaignManagement\BMCStore;
use Microsoft\BingAds\V12\CampaignManagement\ShoppingSetting;
use Microsoft\BingAds\V12\CampaignManagement\CampaignType;

// Specify the Microsoft\BingAds\Auth classes that will be used.
use Microsoft\BingAds\Auth\ServiceClient;
use Microsoft\BingAds\Auth\ServiceClientType;

$GLOBALS['AuthorizationData'] = null;
$GLOBALS['Proxy'] = null;
$GLOBALS['CampaignManagementProxy'] = null;

// Disable WSDL caching.

ini_set("soap.wsdl_cache_enabled", "0");
ini_set("soap.wsdl_cache_ttl", "0");

$PartitionActions = array(); // AdGroupCriterionAction array
$ReferenceId = -1;
$ids = null;

AuthHelper::Authenticate();

$GLOBALS['CampaignManagementProxy'] = new ServiceClient(
    ServiceClientType::CampaignManagementVersion13,
    $GLOBALS['AuthorizationData'],
    AuthHelper::GetApiEnvironment());

$campaigns = (array) CampaignManagementExampleHelper::GetCampaignsByAccountId(65021057, "Shopping", []);

foreach ($campaigns['Campaigns']->Campaign as $campaign) {
    $campaignId = $campaign->Id;
    $campaignName = $campaign->Name;
    $campaignStatus = $campaign->Status;
    $campaignType = $campaign->CampaignType;


    $conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);
    $query = "INSERT IGNORE INTO campaigns (name, bing_id, campaign_type, status) VALUES ('$campaignName', '$campaignId', '$campaignType', '$campaignStatus')";
    $conn->query($query);

    //Now let's get all the ad groups for this campaign
    $adgroups = (array) CampaignManagementExampleHelper::GetAdGroupsByCampaignId($campaignId, []);

    foreach ($adgroups['AdGroups']->AdGroup as $adgroup) {
        $query = "INSERT IGNORE INTO adgroups (name, bing_id, campaign_name, campaign_bing_id) VALUES('$adgroup->Name', '$adgroup->Id', '$campaignName', '$campaignId')";
        $conn->query($query);
    }
}
?>
