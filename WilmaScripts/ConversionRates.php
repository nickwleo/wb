<?php

include  __DIR__ . '/../WilmaConfig.php';

// Create connection
$conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully\r\n";



$result = $conn->query("SELECT a.item_id , a.campaign_name , a.campaign_id , a.adgroup_id , a.adgroup_name , SUM(a.impressions) AS impressions , SUM(a.clicks) AS clicks , SUM(a.conversions) AS conversions , ROUND(SUM(a.cost),2) AS cost , a.average_cpc
        FROM wilma_bing.wilma_products_all_time a WHERE a.conversions > 0 OR a.clicks > 0  GROUP BY a.item_id , a.campaign_name ");

while ($row = $result->fetch_assoc()) {
    
    

    if ((int) $row['conversions'] !== 0)  {
        $sql = "INSERT IGNORE INTO wilma_bing.conversion_rates (mag_sku, campaign_name, campaign_id , adgroup_id , all_time, bucket , updated) VALUES (" . $row['item_id'] . ", '" . $row['campaign_name'] . "', '" . $row['campaign_id'] . "' ,'" . $row['adgroup_id'] . "' , ROUND(" . $row['conversions'] / $row['clicks'] . " ,3) , 'converted' , 1) ON DUPLICATE KEY UPDATE  "
              ." updated = IF( all_time <> VALUES(all_time) , updated + 1 , updated) , all_time = VALUES(all_time) , bucket = 'converted'";
        $conn->query($sql);
    }
    else if ((int) $row['conversions'] == 0 && (int) $row['clicks'] > 0) {
        $sql = "INSERT IGNORE INTO wilma_bing.conversion_rates (mag_sku, campaign_name, campaign_id , adgroup_id , all_time, bucket, updated) VALUES (" . $row['item_id'] . ", '" . $row['campaign_name'] . "', '" . $row['campaign_id'] . "' ,'" . $row['adgroup_id'] . "' , ROUND(" . 1 / $row['clicks'] . " ,3) , 'clicked' , 1) ON DUPLICATE KEY UPDATE "
              ." updated = IF( all_time <> VALUES(all_time) , updated + 1 , updated) , all_time = VALUES(all_time) , bucket = 'clicked' ";
        $conn->query($sql);
    }
}



echo "All Time Completed.\r\n\r\n";

// 7 Day Converted


 $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_7  WHERE conversions > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.7_day = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");
 
 // 7 Day Clicked 
 
  $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_7  WHERE conversions = 0 AND clicks > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.7_day = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");
 
 


echo "7 Day Completed.\r\n\r\n";


// 30 Day Converted


 $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_30  WHERE conversions > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.30_day = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");
 
 // 30 Day Clicked 
 
  $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_30  WHERE conversions = 0 AND clicks > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.30_day = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");


echo "30 Day Completed.\r\n\r\n";

// 90 Day Converted

 $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_90  WHERE conversions > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.90_day = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");
 
 // 90 Day Clicked 
 
  $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_90  WHERE conversions = 0 AND clicks > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.90_day = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");


echo "90 Day Completed.\r\n\r\n";


// 180 Day Converted

 $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_180  WHERE conversions > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.180_day = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");
 
 // 180 Day Clicked 
 
  $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_180  WHERE conversions = 0 AND clicks > 0 GROUP BY item_id , campaign_name) b "
             ."SET a.180_day = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");


echo "180 Day Completed.\r\n\r\n";


// 365 Day Converted

 $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_365  WHERE conversions > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.365_day = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");
 
 // 365 Day Clicked 
 
  $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_365  WHERE conversions = 0 AND clicks > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.365_day = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");





echo "365 Day Completed.\r\n\r\n";


// Real Time Day Converted

 $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_real_time  WHERE conversions > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.real_time = ROUND(b.conversions / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");
 
 // Real Time  Clicked 
 
  $conn->query("UPDATE wilma_bing.conversion_rates a , (SELECT item_id , campaign_name , campaign_id , adgroup_id , adgroup_name , SUM(impressions) AS impressions , SUM(clicks) AS clicks , SUM(conversions) "
             ."AS conversions , ROUND(SUM(cost),2) AS cost , average_cpc FROM wilma_bing.wilma_products_real_time  WHERE conversions = 0 AND clicks > 0  GROUP BY item_id , campaign_name) b "
             ."SET a.real_time = ROUND( 1 / b.clicks ,3)WHERE a.mag_sku = b.item_id AND a.campaign_id = b.campaign_id AND a.updated = 1  ");


echo "Real Time Completed.\r\n\r\n";



$results = $conn->query("SELECT * FROM wilma_bing.conversion_rates WHERE updated = 1 ");

while ($row = $results->fetch_assoc()) {
    $day7 = $row['7_day'];
    $day30 = $row['30_day'];
    $day90 = $row['90_day'];
    $day180 = $row['180_day'];
    $day365 = $row['365_day'];
    $alltime = $row['all_time'];

    $weightingday7 = 0.40;
    $weightingday30 = 0.30;
    $weightingday90 = 0.10;
    $weightingday180 = 0.10;
    $weightingalltime = 0.10;

    if ($day7 == NULL) $weightingday30 = $weightingday30 + $weightingday7;
    if ($day30 == NULL) $weightingday90 = $weightingday90 + $weightingday30;
    if ($day90 == NULL) $weightingday180 = $weightingday180 + $weightingday90;
    if ($day180 == NULL) $weightingalltime = $weightingalltime + $weightingday180;



    if ($row['bucket'] == 'clicked') {
        $weightingday7 = $weightingday30 = $weightingday90 = $weightingday180 = 0.00;
        $weightingalltime = 1.00;
    }


    $weighted_conversion_rate = ($day7 * $weightingday7) + ($day30 * $weightingday30) + ($day90 * $weightingday90) +
        ($day180 * $weightingday180) + ($alltime * $weightingalltime);

    if ($row['bucket'] == 'converted'){ 
        
    $conn->query("UPDATE wilma_bing.conversion_rates SET weighted = ROUND(" . $weighted_conversion_rate . " , 3) WHERE mag_sku = " . $row['mag_sku'] . " AND campaign_id='" . $row['campaign_id'] . "'   ;");
    
    }
    
    if ($row['bucket'] == 'clicked'){ 
        
    $conn->query("UPDATE wilma_bing.conversion_rates SET estimated = ROUND(" . $weighted_conversion_rate . " , 3) WHERE mag_sku = " . $row['mag_sku'] . " AND campaign_id='" . $row['campaign_id'] . "' ; ");

    }
    
 }



?>
