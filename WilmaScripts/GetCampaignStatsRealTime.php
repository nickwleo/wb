<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 18-10-27
 * Time: 6:58 PM
 */

namespace Microsoft\BingAds\Samples\V13;

include  __DIR__ . '/../WilmaConfig.php';
error_reporting(0);

require_once __DIR__ . "/../vendor/autoload.php";

include "AuthHelper.php";
include "ReportRequestLibrary.php";
include  __DIR__ . '/../WilmaConfig.php';

use Microsoft\BingAds\Samples\V13\ReportingExampleHelper;
use Microsoft\BingAds\V13\Reporting\ProductPartitionPerformanceReportRequest;
use Microsoft\BingAds\V13\Reporting\ProductPartitionPerformanceReportColumn;
use Microsoft\BingAds\V13\Reporting\ProductPartitionPerformanceReportFilter;
// Specify the Microsoft\BingAds\Auth classes that will be used.
use Microsoft\BingAds\Auth\ServiceClient;
use Microsoft\BingAds\Auth\ServiceClientType;
use Microsoft\BingAds\V13\Reporting\SubmitGenerateReportRequest;
use Microsoft\BingAds\V13\Reporting\PollGenerateReportRequest;
use Microsoft\BingAds\V13\Reporting\AccountPerformanceReportRequest;
use Microsoft\BingAds\V13\Reporting\AudiencePerformanceReportRequest;
use Microsoft\BingAds\V13\Reporting\KeywordPerformanceReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;
use Microsoft\BingAds\V13\Reporting\AccountThroughAdGroupReportScope;
use Microsoft\BingAds\V13\Reporting\CampaignReportScope;
use Microsoft\BingAds\V13\Reporting\AccountReportScope;
use Microsoft\BingAds\V13\Reporting\ReportTime;
use Microsoft\BingAds\V13\Reporting\ReportTimePeriod;
use Microsoft\BingAds\V13\Reporting\Date;
use Microsoft\BingAds\V13\Reporting\AccountPerformanceReportFilter;
use Microsoft\BingAds\V13\Reporting\KeywordPerformanceReportFilter;
use Microsoft\BingAds\V13\Reporting\DeviceTypeReportFilter;
use Microsoft\BingAds\V13\Reporting\AccountPerformanceReportColumn;
use Microsoft\BingAds\V13\Reporting\AudiencePerformanceReportColumn;
use Microsoft\BingAds\V13\Reporting\KeywordPerformanceReportColumn;
use Microsoft\BingAds\V13\Reporting\ReportRequestStatusType;
use Microsoft\BingAds\V13\Reporting\KeywordPerformanceReportSort;
use Microsoft\BingAds\V13\Reporting\SortOrder;

use SoapVar;
use SoapFault;
use Exception;

$GLOBALS['AuthorizationData'] = null;
$GLOBALS['Proxy'] = null;
$GLOBALS['CampaignManagementProxy'] = null;

// Disable WSDL caching.

ini_set("soap.wsdl_cache_enabled", "0");
ini_set("soap.wsdl_cache_ttl", "0");

$PartitionActions = array(); // AdGroupCriterionAction array
$ReferenceId = -1;
$ids = null;

AuthHelper::Authenticate();

$GLOBALS['ReportingProxy'] = new ServiceClient(
    ServiceClientType::ReportingVersion13,
    $GLOBALS['AuthorizationData'],
    AuthHelper::GetApiEnvironment());

//$GLOBALS['ReportingProxy']->SetAuthorizationData($GLOBALS['AuthorizationData']);
$GLOBALS['Proxy'] = $GLOBALS['ReportingProxy'];

$DownloadPath = __DIR__  . "report.zip";

// Confirm that the download folder exist; otherwise, exit.

$length = strrpos($DownloadPath, '\\');
$folder = substr($DownloadPath, 0, $length);

if (!is_dir(__DIR__))
{
    printf("The output folder, %s, does not exist.\nEnsure that the " .
        "folder exists and try again.", $folder);
    return;
}

try
{
    // Authenticate for Bing Ads services with a Microsoft Account.

    AuthHelper::Authenticate();

    $GLOBALS['ReportingProxy'] = new ServiceClient(ServiceClientType::ReportingVersion13, $GLOBALS['AuthorizationData'], AuthHelper::GetApiEnvironment());

    // You can submit one of the example reports, or build your own.
    /*$report = ReportRequestLibrary::GetAccountPerformanceReportRequest($GLOBALS['AuthorizationData']->AccountId);
    $report = ReportRequestLibrary::GetAudiencePerformanceReportRequest($GLOBALS['AuthorizationData']->AccountId);
    $report = ReportRequestLibrary::GetKeywordPerformanceReportRequest($GLOBALS['AuthorizationData']->AccountId);*/

    $report = new ProductPartitionPerformanceReportRequest();
    $report->Columns = [ProductPartitionPerformanceReportColumn::CampaignName,
        ProductPartitionPerformanceReportColumn::CampaignId,
        ProductPartitionPerformanceReportColumn::AdGroupName,
        ProductPartitionPerformanceReportColumn::AdGroupId,
        ProductPartitionPerformanceReportColumn::Clicks,
        ProductPartitionPerformanceReportColumn::Impressions,
        ProductPartitionPerformanceReportColumn::Conversions,
        ProductPartitionPerformanceReportColumn::Spend,
        ProductPartitionPerformanceReportColumn::AverageCpc,
        ProductPartitionPerformanceReportColumn::PartitionType,
        ProductPartitionPerformanceReportColumn::ProductGroup,
        ProductPartitionPerformanceReportColumn::TimePeriod];

    $report->Format = ReportFormat::Csv;
    $report->ReportName = 'Wilma Shopping Campaign Report';
    $report->ReturnOnlyCompleteData = false;
    $report->Aggregation = ReportAggregation::Yearly;
    $report->Filter = new ProductPartitionPerformanceReportFilter();
    $report->Scope = new AccountThroughAdGroupReportScope();
    $report->Scope->AccountIds = [65021057];
    $report->Time = new ReportTime();

    $date = new \DateTime();
    $formattedTodayDate = $date->format('Y-m-d');
    $Ymd = explode("-", $formattedTodayDate);

    $report->Time->CustomDateRangeEnd = new Date();
    $report->Time->CustomDateRangeEnd->Year=$Ymd[0];
    $report->Time->CustomDateRangeEnd->Month=$Ymd[1];
    $report->Time->CustomDateRangeEnd->Day=$Ymd[2];

    date_sub($date, date_interval_create_from_date_string('0 days'));
    $formattedStartDate = $date->format('Y-m-d');
    $Ymd = explode("-", $formattedStartDate);

    $report->Time->CustomDateRangeStart = new Date();
    $report->Time->CustomDateRangeStart->Year=$Ymd[0];
    $report->Time->CustomDateRangeStart->Month=$Ymd[1];
    $report->Time->CustomDateRangeStart->Day=$Ymd[2];
    //$report->Time->PredefinedTime = ReportTimePeriod::LastYear;

    // SubmitGenerateReport helper method calls the corresponding Bing Ads service operation
    // to request the report identifier. The identifier is used to check report generation status
    // before downloading the report.

    //var_dump(new SoapVar($report, SOAP_ENC_OBJECT, 'ProductPartitionPerformanceReportRequest', $GLOBALS['ReportingProxy']->GetNamespace()));

    //die();

    $reportRequestId = ReportRequestLibrary::SubmitGenerateReport(new SoapVar($report, SOAP_ENC_OBJECT, 'ProductPartitionPerformanceReportRequest', $GLOBALS['ReportingProxy']->GetNamespace()))->ReportRequestId;

    printf("Report Request ID: %s\n\n", $reportRequestId);

    $waitTime = 30 * 1;
    $reportRequestStatus = null;

    // This sample polls every 30 seconds up to 5 minutes.
    // In production you may poll the status every 1 to 2 minutes for up to one hour.
    // If the call succeeds, stop polling. If the call or
    // download fails, the call throws a fault.

    for ($i = 0; $i < 10; $i++)
    {
        sleep($waitTime);

        // PollGenerateReport helper method calls the corresponding Bing Ads service operation
        // to get the report request status.

        $reportRequestStatus = ReportRequestLibrary::PollGenerateReport($reportRequestId)->ReportRequestStatus;

        if ($reportRequestStatus->Status == ReportRequestStatusType::Success ||
            $reportRequestStatus->Status == ReportRequestStatusType::Error)
        {
            break;
        }
    }

    if ($reportRequestStatus != null)
    {
        if ($reportRequestStatus->Status == ReportRequestStatusType::Success)
        {
            $reportDownloadUrl = $reportRequestStatus->ReportDownloadUrl;

            if($reportDownloadUrl == null)
            {
                print "No report data for the submitted request\n";
            }
            else
            {
                printf("Downloading from %s.\n\n", $reportDownloadUrl);
                DownloadFile($reportDownloadUrl, $DownloadPath);
                printf("The report was written to %s.\n", $DownloadPath);
            }

        }
        else if ($reportRequestStatus->Status == ReportRequestStatusType::Error)
        {
            printf("The request failed. Try requesting the report " .
                "later.\nIf the request continues to fail, contact support.\n");
        }
        else  // Pending
        {
            printf("The request is taking longer than expected.\n " .
                "Save the report ID (%s) and try again later.\n",
                $reportRequestId);
        }
    }

}
catch (SoapFault $e)
{
    print "\nLast SOAP request/response:\n";
    printf("Fault Code: %s\nFault String: %s\n", $e->faultcode, $e->faultstring);
    print $GLOBALS['Proxy']->GetWsdl() . "\n";
    print $GLOBALS['Proxy']->GetService()->__getLastRequest()."\n";
    print $GLOBALS['Proxy']->GetService()->__getLastResponse()."\n";

    if (isset($e->detail->AdApiFaultDetail))
    {
        ReportingExampleHelper::OutputAdApiFaultDetail($e->detail->AdApiFaultDetail);

    }
    elseif (isset($e->detail->ApiFaultDetail))
    {
        ReportingExampleHelper::OutputApiFaultDetail($e->detail->ApiFaultDetail);
    }
}
catch (Exception $e)
{
    // Ignore fault exceptions that we already caught.
    if ($e->getPrevious())
    { ; }
    else
    {
        print $e->getCode()." ".$e->getMessage()."\n\n";
        print $e->getTraceAsString()."\n\n";
    }
}

// Using the URL that the PollGenerateReport operation returned,
// send an HTTP request to get the report and write it to the specified
// ZIP file.

function DownloadFile($reportDownloadUrl, $downloadPath)
{
    if (!$reader = fopen($reportDownloadUrl, 'rb'))
    {
        throw new Exception("Failed to open URL " . $reportDownloadUrl . ".");
    }

    if (!$writer = fopen($downloadPath, 'wb'))
    {
        fclose($reader);
        throw new Exception("Failed to create ZIP file " . $downloadPath . ".");
    }

    $bufferSize = 100 * 1024;

    while (!feof($reader))
    {
        if (false === ($buffer = fread($reader, $bufferSize)))
        {
            fclose($reader);
            fclose($writer);
            throw new Exception("Read operation from URL failed.");
        }

        if (fwrite($writer, $buffer) === false)
        {
            fclose($reader);
            fclose($writer);
            $exception = new Exception("Write operation to ZIP file failed.");
        }
    }

    fclose($reader);
    fflush($writer);
    fclose($writer);
}

$zip = new \ZipArchive;
$unzippedFileName = "";
if ($zip->open('WilmaScriptsreport.zip') === TRUE) {
    $zip->extractTo(getcwd());
    $unzippedFileName = $zip->getNameIndex(0);
    $zip->close();
    echo($unzippedFileName);
} else {
    echo 'failed';
}

$csvFile = file($unzippedFileName);
$data = [];
foreach ($csvFile as $line) {
    $data[] = str_getcsv($line);
}

$startIndex = 0;

foreach ($data as $csvLine) {
    $startIndex++;
    if ($csvLine[0] == "CampaignName") break;
}

$conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);
$query = "INSERT INTO wilma_products_real_time (item_id, campaign_id, campaign_name, adgroup_id, adgroup_name, impressions, clicks, conversions, average_cpc, cost) VALUES ";

//Now start reading CSV data

$addComma = false;

do {
    $productId = preg_replace("/[^0-9]/", "",$data[$startIndex][10]);
    $product = $data[$startIndex];

    foreach ($product as $key => $field) {
        if (!is_numeric($field)) $product[$key] = "0";
    }

    if(is_numeric($productId)) {
        if ($addComma) $query .= ",";
        $query .= "('$productId', '$product[1]', '$product[0]', '$product[3]', '$product[2]', '$product[5]', '$product[4]', '$product[6]', '$product[8]', '$product[7]')";
        $addComma = true;
    }

    $startIndex++;

} while ($data[$startIndex+1][0] != "");

$query .= " ON DUPLICATE KEY UPDATE impressions = VALUES(impressions), clicks = VALUES(clicks), conversions = VALUES(conversions), average_cpc = VALUES(average_cpc), cost = VALUES(cost)";

echo($query);

$conn->query($query);

unlink('WilmaScriptsreport.zip');
unlink($unzippedFileName);
