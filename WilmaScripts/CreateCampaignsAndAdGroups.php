<?php

namespace Microsoft\BingAds\Samples\V13;

// For more information about installing and using the Bing Ads PHP SDK,
// see https://go.microsoft.com/fwlink/?linkid=838593.

ini_set('memory_limit','4096M');

require_once __DIR__ . "/../vendor/autoload.php";

include __DIR__ . "/AuthHelper.php";
include "CampaignManagementExampleHelper.php";
include  __DIR__ . '/../WilmaConfig.php';

use Microsoft\BingAds\V13\CampaignManagement\AdGroupCriterion;
use SoapVar;
use SoapFault;
use Exception;

// Specify the Microsoft\BingAds\V13\CampaignManagement classes that will be used.
use Microsoft\BingAds\V13\CampaignManagement\Campaign;
use Microsoft\BingAds\V13\CampaignManagement\BudgetLimitType;
use Microsoft\BingAds\V13\CampaignManagement\BMCStore;
use Microsoft\BingAds\V13\CampaignManagement\ShoppingSetting;
use Microsoft\BingAds\V13\CampaignManagement\CampaignType;
use Microsoft\BingAds\V13\CampaignManagement\AdGroup;
use Microsoft\BingAds\V13\CampaignManagement\Date;
use Microsoft\BingAds\V13\CampaignManagement\ProductAd;
use Microsoft\BingAds\V13\CampaignManagement\CampaignCriterionType;
use Microsoft\BingAds\V13\CampaignManagement\ProductScope;
use Microsoft\BingAds\V13\CampaignManagement\BiddableCampaignCriterion;
use Microsoft\BingAds\V13\CampaignManagement\ProductCondition;
use Microsoft\BingAds\V13\CampaignManagement\ProductPartitionType;
use Microsoft\BingAds\V13\CampaignManagement\AdGroupCriterionType;
use Microsoft\BingAds\V13\CampaignManagement\BiddableAdGroupCriterion;
use Microsoft\BingAds\V13\CampaignManagement\ItemAction;
use Microsoft\BingAds\V13\CampaignManagement\AdGroupCriterionAction;
use Microsoft\BingAds\V13\CampaignManagement\NegativeAdGroupCriterion;
use Microsoft\BingAds\V13\CampaignManagement\ProductPartition;
use Microsoft\BingAds\V13\CampaignManagement\FixedBid;
use Microsoft\BingAds\V13\CampaignManagement\Bid;
use Microsoft\BingAds\V13\CampaignManagement\BatchErrorCollection;

// Specify the Microsoft\BingAds\Auth classes that will be used.
use Microsoft\BingAds\Auth\ServiceClient;
use Microsoft\BingAds\Auth\ServiceClientType;

// Specify the Microsoft\BingAds\Samples classes that will be used.
use Microsoft\BingAds\Samples\V13\AuthHelper;
use Microsoft\BingAds\Samples\V13\CampaignManagementExampleHelper;

$GLOBALS['AuthorizationData'] = null;
$GLOBALS['Proxy'] = null;
$GLOBALS['CampaignManagementProxy'] = null;

// Disable WSDL caching.

ini_set("soap.wsdl_cache_enabled", "0");
ini_set("soap.wsdl_cache_ttl", "0");

$PartitionActions = array(); // AdGroupCriterionAction array
$ReferenceId = -1;
$ids = null;

try
{
    // Authenticate for Bing Ads services with a Microsoft Account.

    AuthHelper::Authenticate();

    $conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);
    $query = "SELECT * FROM campaigns_to_create WHERE created=0";
    $campaignsToCreate = $conn->query($query);

    while ($newCampaign = mysqli_fetch_array($campaignsToCreate)){

        $GLOBALS['CampaignManagementProxy'] = new ServiceClient(
            ServiceClientType::CampaignManagementVersion13,
            $GLOBALS['AuthorizationData'],
            AuthHelper::GetApiEnvironment());

        $stores= CampaignManagementExampleHelper::GetBMCStoresByCustomerId(false)->BMCStores->BMCStore;

        if (!isset($stores))
        {
            printf("CustomerId %d does not have any registered BMC stores.\n\n", $CustomerId);
            return;
        }

        // Create a Bing Shopping campaign using the ID of the first store in the list.
        $settings = array();

        $shoppingSetting = new ShoppingSetting();
        $shoppingSetting->Priority = 0;
        $shoppingSetting->SalesCountryCode = "US";
        $shoppingSetting->StoreId = $stores[0]->Id;

        $encodedSetting = new SoapVar(
            $shoppingSetting,
            SOAP_ENC_OBJECT,
            'ShoppingSetting',
            $GLOBALS['CampaignManagementProxy']->GetNamespace());
        $settings[] = $encodedSetting;

        $campaigns = array();
        $campaign = new Campaign();
        $campaign->Name = $newCampaign['name'];
        $campaign->Description = $newCampaign['description'];
        $campaign->BudgetType = BudgetLimitType::DailyBudgetStandard;
        $campaign->DailyBudget = $newCampaign['daily_budget'];
        $campaign->Settings = $settings;
        $campaign->CampaignType = CampaignType::Shopping;
        $campaign->TimeZone = "PacificTimeUSCanadaTijuana";
        $campaigns[] = $campaign;

        // Create the ad group that will have the product partitions.

        $adGroups = array();

        for ($j = 1;$j<101;$j++) {
            date_default_timezone_set('UTC');
            $endDate = new Date();
            $endDate->Day = 31;
            $endDate->Month = 12;
            $endDate->Year = date("Y");
            $adGroup = new AdGroup();
            $adGroup->Name = $newCampaign['name'] . $j;
            echo ("AdGroup Name : " . $adGroup->Name);
            $adGroup->StartDate = null;
            $adGroup->EndDate = $endDate;
            $adGroup->CpcBid = new Bid();
            $adGroup->CpcBid->Amount = $newCampaign['adgroup_default_cpc'];
            $adGroup->Language = "English";

            //$adGroups[] = $adGroup;

            array_push($adGroups, $adGroup);

            $ads[$j-1] = array();
            $ad = new ProductAd();
            $ad = new ProductAd();
            $encodedAd = new SoapVar(
                $ad,
                SOAP_ENC_OBJECT,
                'ProductAd',
                $GLOBALS['CampaignManagementProxy']->GetNamespace());
            $ads[$j-1][] = $encodedAd;
        }

        print "AddCampaigns\n";
        $addCampaignsResponse = CampaignManagementExampleHelper::AddCampaigns($GLOBALS['AuthorizationData']->AccountId, $campaigns);
        $nillableCampaignIds = $addCampaignsResponse->CampaignIds;
        CampaignManagementExampleHelper::OutputArrayOfLong($nillableCampaignIds);
        if(isset($addCampaignsResponse->PartialErrors->BatchError)){
            CampaignManagementExampleHelper::OutputArrayOfBatchError($addCampaignsResponse->PartialErrors);
        }

        print "AddAdGroups\n";
        $addAdGroupsResponse = CampaignManagementExampleHelper::AddAdGroups($nillableCampaignIds->long[0], $adGroups, false);
        $nillableAdGroupIds = $addAdGroupsResponse->AdGroupIds;
        CampaignManagementExampleHelper::OutputArrayOfLong($nillableAdGroupIds);
        if(isset($addAdGroupsResponse->PartialErrors->BatchError)){
            CampaignManagementExampleHelper::OutputArrayOfBatchError($addAdGroupsResponse->PartialErrors);
        }

        for ($j = 0;$j < 100; $j++) {
            print "AddAds\n";
            $addAdsResponse = CampaignManagementExampleHelper::AddAds($nillableAdGroupIds->long[$j], $ads[$j]);
            $nillableAdIds = $addAdsResponse->AdIds;
            CampaignManagementExampleHelper::OutputArrayOfLong($nillableAdIds);
            if (isset($addAdsResponse->PartialErrors->BatchError)) {
                CampaignManagementExampleHelper::OutputArrayOfBatchError($addAdsResponse->PartialErrors);
            }
        }

        // NICK'S NOTE TO SELF - Need to implement Logic before this point to ensure that no more than 10,000/20,000 go to each adgroup.
        // Need to limit number of partitition actions in array at a time

        AddAndUpdateAdGroupCriterion(
            $GLOBALS['AuthorizationData']->AccountId,
            $PartitionActions,
            $nillableAdGroupIds->long);

    }

}
catch (SoapFault $e)
{
    print "\nLast SOAP request/response:\n";
    printf("Fault Code: %s\nFault String: %s\n", $e->faultcode, $e->faultstring);
    print $GLOBALS['Proxy']->GetWsdl() . "\n";
    print $GLOBALS['Proxy']->GetService()->__getLastRequest()."\n";
    print $GLOBALS['Proxy']->GetService()->__getLastResponse()."\n";

    if (isset($e->detail->AdApiFaultDetail))
    {
        CampaignManagementExampleHelper::OutputAdApiFaultDetail($e->detail->AdApiFaultDetail);

    }
    elseif (isset($e->detail->ApiFaultDetail))
    {
        CampaignManagementExampleHelper::OutputApiFaultDetail($e->detail->ApiFaultDetail);
    }
    elseif (isset($e->detail->EditorialApiFaultDetail))
    {
        CampaignManagementExampleHelper::OutputEditorialApiFaultDetail($e->detail->EditorialApiFaultDetail);
    }
}
catch (Exception $e)
{
    // Ignore fault exceptions that we already caught.
    if ($e->getPrevious())
    { ; }
    else
    {
        print $e->getCode()." ".$e->getMessage()."\n\n";
        print $e->getTraceAsString()."\n\n";
    }
}

// Add a criterion to the ad group and then update it.

function AddAndUpdateAdGroupCriterion($accountId, &$actions, $adGroupIdArray)
{

    include  __DIR__ . '/../WilmaConfig.php';

    $conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);
    $query = "SELECT mag_sku from wilma_bing.master_product WHERE qty > 0";
    $products = $conn->query($query);

    $adGroupArrayIndex = 0;

    $productsCount = $products->num_rows;

    $ctr = 0;

    while ($ctr < $productsCount) {

        $count = 0;

        $adGroupId = $adGroupIdArray[$adGroupArrayIndex];

        $adGroupArrayIndex++;

        // Add a biddable criterion as the root.

        $actions = array();

        $condition = new ProductCondition();
        $condition->Operand = "All";
        $condition->Attribute = null;

        $root = AddPartition(
            $adGroupId,
            null,
            $condition,
            ProductPartitionType::Subdivision,
            GetFixedBid(0.35),
            false,
            $actions);

        // Adding specific items

        $condition = new ProductCondition();
        $condition->Operand = "ID";
        $condition->Attribute = null;

        $node2 = AddPartition(
            $adGroupId,
            $root,
            $condition,
            ProductPartitionType::Unit,
            GetFixedBid(0.10),
            false,
            $actions);

        while ($ctr < $productsCount && $count < 2500) {//3 = max for ad group in testing

            $product = mysqli_fetch_array($products);

            $ctr++;

            $condition = new ProductCondition();
            $condition->Operand = "ID";
            $condition->Attribute = $product["mag_sku"];
            //$productConditions[] = $condition;
            $node = AddPartition(
                $adGroupId,
                $root,
                $condition,
                ProductPartitionType::Unit,
                GetFixedBid(0.12),
                false,
                $actions);

            $count++;
        }

        printf("Applying a biddable criterions as the root...\n\n");
        $applyPartitionActionsResponse = CampaignManagementExampleHelper::ApplyProductPartitionActions($actions);

        CampaignManagementExampleHelper::OutputArrayOfLong($applyPartitionActionsResponse->AdGroupCriterionIds);
        if (isset($applyPartitionActionsResponse->PartialErrors->BatchError)) {
            CampaignManagementExampleHelper::OutputArrayOfBatchError($applyPartitionActionsResponse->PartialErrors);
        }

        GetBingIds($adGroupId);
    }

}

// Add a criterion to the ad group and then update it.

function GetBingIds($adGroupId)
{

    $adGroupCriterions = CampaignManagementExampleHelper::GetAdGroupCriterionsByIds(
        null,
        $adGroupId,
        AdGroupCriterionType::ProductPartition)->AdGroupCriterions;

    printf("Outputting the ad group's product partition; contains only the tree root node\n\n");
    //OutputProductPartitions($adGroupCriterions);

    $adCriterionArray = (array)$adGroupCriterions;

    require __DIR__ . '/../WilmaConfig.php';

    $conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);

    foreach ($mag_sku = $adCriterionArray["AdGroupCriterion"] as $Criterion) {

        $mag_sku = $Criterion->Criterion->Condition->Attribute;

        $bing_id = $Criterion->Id;//integer

        $adGroupId = $Criterion->AdGroupId;//integer

        $query = "INSERT IGNORE INTO products_on_bing (mag_sku, bing_id, adgroup_id)" .
            " VALUES ('$mag_sku', '$bing_id', '$adGroupId')";

        $conn->query($query);

    }

    //$conn->query("UPDATE products_on_bing, adgroups SET products_on_bing.adgroup_name=adgroups.name, products_on_bing.campaign_name=adgroups.campaign_name WHERE products_on_bing.adgroup_id = adgroups.bing_id");

}

function AddBranchAndLeafCriterion($accountId, &$actions, $adGroupId)
{
    $actions = array();  // clear

    $adGroupCriterions = CampaignManagementExampleHelper::GetAdGroupCriterionsByIds(
        null,
        $adGroupId,
        AdGroupCriterionType::ProductPartition)->AdGroupCriterions;

    $existingRoot = GetRootNode($adGroupCriterions);

    if (isset($existingRoot))
    {
        $nodeToDelete = new BiddableAdGroupCriterion();
        $nodeToDelete->Id = $existingRoot->Id;
        $nodeToDelete->AdGroupId = $existingRoot->AdGroupId;

        $encodedNodeToDelete = new SoapVar(
            $nodeToDelete,
            SOAP_ENC_OBJECT,
            'BiddableAdGroupCriterion',
            $GLOBALS['CampaignManagementProxy']->GetNamespace());

        AddPartitionAction($encodedNodeToDelete, ItemAction::Delete, $actions);
    }

    $condition = new ProductCondition();
    $condition->Operand = "All";
    $condition->Attribute = null;

    $root = AddPartition(
        $adGroupId,
        null,
        $condition,
        ProductPartitionType::Subdivision,
        null,
        false,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "CategoryL1";
    $condition->Attribute = "Animals & Pet Supplies";

    $animalsSubdivision = AddPartition(
        $adGroupId,
        $root,
        $condition,
        ProductPartitionType::Subdivision,
        null,
        false,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "CategoryL2";
    $condition->Attribute = "Pet Supplies";

    $petSuppliesSubdivision = AddPartition(
        $adGroupId,
        $animalsSubdivision,
        $condition,
        ProductPartitionType::Subdivision,
        null,
        false,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "Brand";
    $condition->Attribute = "Brand A";

    $brandA = AddPartition(
        $adGroupId,
        $petSuppliesSubdivision,
        $condition,
        ProductPartitionType::Unit,
        GetFixedBid(0.35),
        false,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "Brand";
    $condition->Attribute = "Brand B";

    $brandB = AddPartition(
        $adGroupId,
        $petSuppliesSubdivision,
        $condition,
        ProductPartitionType::Unit,
        null,
        true,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "Brand";
    $condition->Attribute = null;

    $otherBrands = AddPartition(
        $adGroupId,
        $petSuppliesSubdivision,
        $condition,
        ProductPartitionType::Unit,
        GetFixedBid(0.35),
        false,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "CategoryL2";
    $condition->Attribute = null;

    $otherPetSupplies = AddPartition(
        $adGroupId,
        $animalsSubdivision,
        $condition,
        ProductPartitionType::Unit,
        GetFixedBid(0.35),
        false,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "CategoryL1";
    $condition->Attribute = "Electronics";

    $electronics = AddPartition(
        $adGroupId,
        $root,
        $condition,
        ProductPartitionType::Unit,
        GetFixedBid(0.35),
        false,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "CategoryL1";
    $condition->Attribute = null;

    $otherCategoryL1 = AddPartition(
        $adGroupId,
        $root,
        $condition,
        ProductPartitionType::Unit,
        GetFixedBid(0.35),
        false,
        $actions);

    printf("Applying product partitions to the ad group...\n\n");
    $applyPartitionActionsResponse = CampaignManagementExampleHelper::ApplyProductPartitionActions($actions);

    $adGroupCriterions = CampaignManagementExampleHelper::GetAdGroupCriterionsByIds(
        null,
        $adGroupId,
        AdGroupCriterionType::ProductPartition)->AdGroupCriterions;

    printf("The product partition group tree now has 9 nodes\n\n");
    OutputProductPartitions($adGroupCriterions);

    return $applyPartitionActionsResponse;
}


// Deletes and updates branch and leaf criterion.

function UpdateBranchAndLeafCriterion(&$actions, $accountId, $adGroupId, $rootId, $electronicsCriterionId)
{
    $actions = array(); // clear;

    $electronics = new BiddableAdGroupCriterion();
    $electronics->Id = $electronicsCriterionId;
    $electronics->AdGroupId = $adGroupId;
    $encodedNodeToDelete = new SoapVar(
        $electronics,
        SOAP_ENC_OBJECT,
        'BiddableAdGroupCriterion',
        $GLOBALS['CampaignManagementProxy']->GetNamespace());

    AddPartitionAction($encodedNodeToDelete, ItemAction::Delete, $actions);

    $parent = new BiddableAdGroupCriterion();
    $parent->Id = $rootId;
    $encodedParent = new SoapVar(
        $parent,
        SOAP_ENC_OBJECT,
        'BiddableAdGroupCriterion',
        $GLOBALS['CampaignManagementProxy']->GetNamespace());

    $condition = new ProductCondition();
    $condition->Operand = "CategoryL1";
    $condition->Attribute = "Electronics";

    $electronicsSubdivision = AddPartition(
        $adGroupId,
        $encodedParent,
        $condition,
        ProductPartitionType::Subdivision,
        null,
        false,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "Brand";
    $condition->Attribute = "Brand C";

    $brandC = AddPartition(
        $adGroupId,
        $electronicsSubdivision,
        $condition,
        ProductPartitionType::Unit,
        GetFixedBid(0.35),
        false,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "Brand";
    $condition->Attribute = "Brand D";

    $brandD = AddPartition(
        $adGroupId,
        $electronicsSubdivision,
        $condition,
        ProductPartitionType::Unit,
        GetFixedBid(0.35),
        false,
        $actions);

    $condition = new ProductCondition();
    $condition->Operand = "Brand";
    $condition->Attribute = null;

    $otherElectronicBrands = AddPartition(
        $adGroupId,
        $electronicsSubdivision,
        $condition,
        ProductPartitionType::Unit,
        GetFixedBid(0.35),
        false,
        $actions);

    printf("\nUpdating the electronics partition...\n");
    $applyPartitionActionsResponse = CampaignManagementExampleHelper::ApplyProductPartitionActions($actions);

    $adGroupCriterions = CampaignManagementExampleHelper::GetAdGroupCriterionsByIds(
        null,
        $adGroupId,
        AdGroupCriterionType::ProductPartition)->AdGroupCriterions;

    printf("\nThe product partition group tree now has 12 nodes\n");
    OutputProductPartitions($adGroupCriterions);
}


// Get the root criterion node.

function GetRootNode($adGroupCriterions)
{
    $rootNode = null;

    foreach ($adGroupCriterions->AdGroupCriterion as $adGroupCriterion)
    {
        if (empty($adGroupCriterion->Criterion->ParentCriterionId))
        {
            $rootNode = $adGroupCriterion;
            break;
        }
    }

    return $rootNode;
}

// Gets a fixed bid object with the specified bid amount.

function GetFixedBid($bidAmount)
{
    $fixedBid = new FixedBid();
    $fixedBid->Amount = $bidAmount;

    $encodedFixedBid = new SoapVar(
        $fixedBid,
        SOAP_ENC_OBJECT,
        'FixedBid',
        $GLOBALS['CampaignManagementProxy']->GetNamespace());

    return $encodedFixedBid;
}

// Adds a criterion action to the list of actions.

function AddPartitionAction($criterion, $itemAction, &$actions)
{
    $partitionAction = new AdGroupCriterionAction();
    $partitionAction->Action = $itemAction;
    $partitionAction->AdGroupCriterion = $criterion;

    $actions[] = $partitionAction;
}

// Adds either a negative or biddable partition criterion.

function AddPartition(
    $adGroupId,
    $parent,  		// AdGroupCriterion
    $condition,  	// ProductCondition
    $partitionType, // ProductPartitionType
    $bid, 			// FixedBid
    $isNegative,
    &$actions)
{
    global $ReferenceId;

    $adGroupCriterion = null;

    if ($isNegative)
    {
        $adGroupCriterion = new NegativeAdGroupCriterion();
    }
    else
    {
        $adGroupCriterion = new BiddableAdGroupCriterion();
        $adGroupCriterion->CriterionBid = $bid;
    }

    $adGroupCriterion->AdGroupId = $adGroupId;

    // Parent is encoded, so dereference enc_value.

    $criterion = new ProductPartition();
    $criterion->Condition = $condition;
    $criterion->ParentCriterionId = (($parent != null) ? $parent->enc_value->Id : null);

    if ($partitionType === ProductPartitionType::Subdivision)
    {
        $criterion->PartitionType = ProductPartitionType::Subdivision;  // Branch
        $adGroupCriterion->Id = $ReferenceId--;
    }
    else
    {
        $criterion->PartitionType = ProductPartitionType::Unit;  // Leaf
    }

    $encodedCriterion = new SoapVar(
        $criterion,
        SOAP_ENC_OBJECT,
        'ProductPartition',
        $GLOBALS['CampaignManagementProxy']->GetNamespace());

    $adGroupCriterion->Criterion = $encodedCriterion;

    if ($isNegative)
    {
        $encodedAdGroupCriterion = new SoapVar(
            $adGroupCriterion,
            SOAP_ENC_OBJECT,
            'NegativeAdGroupCriterion',
            $GLOBALS['CampaignManagementProxy']->GetNamespace());
    }
    else
    {
        $encodedAdGroupCriterion = new SoapVar(
            $adGroupCriterion,
            SOAP_ENC_OBJECT,
            'BiddableAdGroupCriterion',
            $GLOBALS['CampaignManagementProxy']->GetNamespace());
    }

    AddPartitionAction($encodedAdGroupCriterion, ItemAction::Add, $actions);

    return $encodedAdGroupCriterion;
}

// Generates the ad group's partition tree that we then print.

function OutputProductPartitions($adGroupCriterions)
{
    $childBranches = array(); // Hash map (Long, List(AdGroupCriterion));
    $treeRoot = null;

    foreach ($adGroupCriterions->AdGroupCriterion as $adGroupCriterion)
    {
        $partition = $adGroupCriterion->Criterion;
        $childBranches[$adGroupCriterion->Id] = array();

        if (!empty($partition->ParentCriterionId))
        {
            $childBranches[$partition->ParentCriterionId][] = $adGroupCriterion;
        }
        else
        {
            $treeRoot = $adGroupCriterion;
        }
    }

    OutputProductPartitionTree($treeRoot, $childBranches, 0);
}


// Output the partition tree.

function OutputProductPartitionTree(
    $node,
    $childBranches,  // hash map (Long, List(AdGroupCriterion))
    $treeLevel)
{
    $criterion = $node->Criterion;  // ProductPartition

    printf("%" . (($treeLevel > 0) ? $treeLevel * 4 : "") . "s%s\n",
        "",
        $criterion->PartitionType);

    printf("%" . (($treeLevel > 0) ? $treeLevel * 4 : "") . "s%s%s\n",
        "",
        "ParentCriterionId: ",
        $criterion->ParentCriterionId);

    printf("%" . (($treeLevel > 0) ? $treeLevel * 4 : "") . "s%s%s\n",
        "",
        "Id: ",
        $node->Id);

    if ($criterion->PartitionType === ProductPartitionType::Unit)
    {
        if ($node->Type === "BiddableAdGroupCriterion") //instanceof BiddableAdGroupCriterion)
        {
            printf("%" . (($treeLevel > 0) ? $treeLevel * 4 : "") . "s%s%.2f\n",
                "",
                "Bid amount: ",
                $node->CriterionBid->Amount);  // ((FixedBid)((BiddableAdGroupCriterion)

        }
        else
        {
            if ($node->Type === "NegativeAdGroupCriterion")  // node instanceof NegativeAdGroupCriterion
            {
                printf("%" . $treeLevel * 4 . "s%s\n",
                    "",
                    "Not bidding on this condition");
            }
        }
    }

    $nullAttribute = (!empty($criterion->ParentCriterionId)) ? "(All Others)" : "(Tree Root)";

    printf("%" . (($treeLevel > 0) ? $treeLevel * 4 : "") . "s%s%s\n",
        "",
        "Attribute: ",
        (empty($criterion->Condition->Attribute)) ?
            $nullAttribute : $criterion->Condition->Attribute);

    printf("%" . (($treeLevel > 0) ? $treeLevel * 4 : "") . "s%s%s\n\n",
        "",
        "Condition: ",
        $criterion->Condition->Operand);

    foreach ($childBranches[$node->Id] as $childNode)  // AdGroupCriterion
    {
        OutputProductPartitionTree($childNode, $childBranches, $treeLevel + 1);
    }
}
