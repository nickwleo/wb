<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 29/06/18
 * Time: 9:08 PM
 */

include  __DIR__ . '/../WilmaConfig.php';

// Create connection
$conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
echo "\r\nConnected successfully\r\n";

// INSERT Items Not In campaign tables and sync capaign tables

//MDMB

$conn->query("INSERT IGNORE INTO wilma_bing.cm_mdmb  (item_id , campaign_id , adgroup_id , bucket , adgroup_name , impressions , clicks , conversions , cost , average_cpc)
SELECT sku , campaign_id , adgroup_id , current_bucket , adgroup_name , total_impressions , total_clicks , total_conversions , total_cost , average_cpc 
FROM wilma_bing.bid_summary WHERE campaign_id = 352174697 ON DUPLICATE KEY UPDATE wilma_bing.cm_mdmb.bucket = wilma_bing.bid_summary.current_bucket ,
wilma_bing.cm_mdmb.impressions = wilma_bing.bid_summary.total_impressions , wilma_bing.cm_mdmb.clicks = wilma_bing.bid_summary.total_clicks , 
wilma_bing.cm_mdmb.conversions = wilma_bing.bid_summary.total_conversions , wilma_bing.cm_mdmb.cost = wilma_bing.bid_summary.total_cost , 
wilma_bing.cm_mdmb.average_cpc = wilma_bing.bid_summary.average_cpc ;");



//MDPART

$conn->query("INSERT IGNORE INTO wilma_bing.cm_mdpart  (item_id , campaign_id , adgroup_id , bucket , adgroup_name , impressions , clicks , conversions , cost , average_cpc)
SELECT sku , campaign_id , adgroup_id , current_bucket , adgroup_name , total_impressions , total_clicks , total_conversions , total_cost , average_cpc 
FROM wilma_bing.bid_summary WHERE campaign_id = 352168817 ON DUPLICATE KEY UPDATE wilma_bing.cm_mdpart.bucket = wilma_bing.bid_summary.current_bucket ,
wilma_bing.cm_mdpart.impressions = wilma_bing.bid_summary.total_impressions , wilma_bing.cm_mdpart.clicks = wilma_bing.bid_summary.total_clicks , 
wilma_bing.cm_mdpart.conversions = wilma_bing.bid_summary.total_conversions , wilma_bing.cm_mdpart.cost = wilma_bing.bid_summary.total_cost , 
wilma_bing.cm_mdpart.average_cpc = wilma_bing.bid_summary.average_cpc
 ;");



//MedicalPLA

$conn->query("INSERT IGNORE INTO wilma_bing.cm_mdpla  (item_id , campaign_id , adgroup_id , bucket , adgroup_name , impressions , clicks , conversions , cost , average_cpc)
SELECT sku , campaign_id , adgroup_id , current_bucket , adgroup_name , total_impressions , total_clicks , total_conversions , total_cost , average_cpc 
FROM wilma_bing.bid_summary WHERE campaign_id = 352174436 ON DUPLICATE KEY UPDATE wilma_bing.cm_mdpla.bucket = wilma_bing.bid_summary.current_bucket ,
wilma_bing.cm_mdpla.impressions = wilma_bing.bid_summary.total_impressions , wilma_bing.cm_mdpla.clicks = wilma_bing.bid_summary.total_clicks , 
wilma_bing.cm_mdpla.conversions = wilma_bing.bid_summary.total_conversions , wilma_bing.cm_mdpla.cost = wilma_bing.bid_summary.total_cost , 
wilma_bing.cm_mdpla.average_cpc = wilma_bing.bid_summary.average_cpc ;");

//MDTAB

$conn->query("INSERT IGNORE INTO wilma_bing.cm_mdtab  (item_id , campaign_id , adgroup_id , bucket , adgroup_name , impressions , clicks , conversions , cost , average_cpc)
SELECT sku , campaign_id , adgroup_id , current_bucket , adgroup_name , total_impressions , total_clicks , total_conversions , total_cost , average_cpc 
FROM wilma_bing.bid_summary WHERE campaign_id = 352174434 ON DUPLICATE KEY UPDATE wilma_bing.cm_mdtab.bucket = wilma_bing.bid_summary.current_bucket ,
wilma_bing.cm_mdtab.impressions = wilma_bing.bid_summary.total_impressions , wilma_bing.cm_mdtab.clicks = wilma_bing.bid_summary.total_clicks , 
wilma_bing.cm_mdtab.conversions = wilma_bing.bid_summary.total_conversions , wilma_bing.cm_mdtab.cost = wilma_bing.bid_summary.total_cost , 
wilma_bing.cm_mdtab.average_cpc = wilma_bing.bid_summary.average_cpc ;");




//include('prep_conversion_rates.php');

?>
